//
//  ProductRegistration.swift
//  AIM_2
//
//  Created by Manuel Garcia on 08/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ProductRegistration: View {
    @State var productName: String = ""

    var body: some View {
                ScrollView{
                    VStack {
                        Rectangle()
                        .fill(Color.gray)
                        .frame(height: 300)
                        
//                        CircleImage(image: nil)
//                            .offset(x: 0, y: -130)
//                            .padding(.bottom, -130)

                        VStack(alignment: .leading) {
                            
                            TextField("Nombre del producto", text:$productName).modifier(FieldModifier())
                            
                            TextField("Descripción del producto", text:$productName).modifier(FieldModifier())
                                .lineLimit(nil)
                            
//                            UITextView("Hola")
//                            HStack {
//                                Text("product.name")
//                                    .font(.title)
//                                Spacer()
//                                Text(String("warehouse.amount"))
//                                    .bold()
//                                    .font(.largeTitle)
//                                    .fixedSize(horizontal: false, vertical: true)
//                            }
//        //
//                            HStack(alignment: .top) {
//                                Text("product.line")
//                                    .fixedSize(horizontal: false, vertical: true)
//                                    .font(.subheadline)
//                                Spacer()
//                                Text("product.movementUnits.entry")
//                                    .fixedSize(horizontal: false, vertical: true)
//                                    .font(.subheadline)
//                            }
//        //
//                            Text("product.description")
//                                .fixedSize(horizontal: false, vertical: true)
//                                .padding(.top, 50)
//        //
//                            Text("Se empaca en \("product.movementUnits.out") con tasa de conversion de:  \("product.unitFactor")") T                        .lineLimit(nil)
//                                .fixedSize(horizontal: false, vertical: true)
//                                .font(.subheadline)
//                                .padding(.top,50)
//
//
//        //                    }
//        //                .padding()
                        
                        }.padding()
        //                Spacer()
                        }.fixedSize(horizontal: false, vertical: true)
                    
                }
    }
}

struct FieldModifier : ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
            .cornerRadius(4.0)
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 15, trailing: 0))
    }
}

struct ProductRegistration_Previews: PreviewProvider {
    static var previews: some View {
        ProductRegistration()
    }
}
