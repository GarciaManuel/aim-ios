//
//  UnitTypeRow.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct UnitTypeRow: View {
    var unitType: UnitType

    var body: some View {
        HStack {
            Text(String(unitType.name.uppercased().prefix(2) + ": "))
                .bold()
                .fixedSize(horizontal: false, vertical: true)
            Text(unitType.name)
                .fixedSize(horizontal: false, vertical: true)
            Spacer()

        }
        
    }
}

struct UnitTypeRow_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            UnitTypeRow(unitType: unitTypeData[0])
            UnitTypeRow(unitType: unitTypeData[1])
            UnitTypeRow(unitType: unitTypeData[2])
        }
        .previewLayout(.fixed(width: 300, height: 70))

        
    }
}
