//
//  WarehouseDetail.swift
//  AIM_2
//
//  Created by Manuel Garcia on 24/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct WarehouseDetail: View {
    var warehouse: Warehouse

    var body: some View {
        VStack {
            MapView(coordinate: warehouse.locationCoordinate)
                .frame(height: 300)
            CircleImage(image: warehouse.image)
                .offset(x: 0, y: -130)
                .padding(.bottom, -130)

            HStack {
                VStack(alignment: .leading) {
                    
                    Text(warehouse.name)
                        .font(.title)
                        .fixedSize(horizontal: false, vertical: true)

                    HStack(alignment: .top) {
                        Text(warehouse.address)
                            .font(.subheadline)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()
    //                    Text(landmark.state)
    //                        .font(.subheadline)
                    }
                }
                .padding()
                NavigationLink(destination: UnitTypeList(),
                               label: { Text("Unidades")
                                .foregroundColor(Color.white)
                                    .padding()
                                .background(Color.orange)
                                    .cornerRadius(5.0)
                })
//                Button(action: {
//                    // What to perform
//                }) {
//                    Text("Inventario")
//                        .foregroundColor(Color.white)
//                        .bold()
//
//                }


            }.padding()
            
            Spacer()
            HStack{

                NavigationLink(destination: ProductList(),
                               label: { Text("Inventario existente")
                                .foregroundColor(Color.white)
                                    .padding()
                                    .background(Color.blue)
                                    .cornerRadius(5.0)
                })
            }

            Spacer()
        }
            
        .navigationBarTitle(Text(verbatim: warehouse.name), displayMode: .inline)
    
    }
}

struct WarehouseDetail_Previews: PreviewProvider {
    static var previews: some View {
        WarehouseDetail(warehouse: warehouseData[0])
    }
}
