//
//  ProductList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 03/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ProductList: View {
    @State private var searchTerm: String = ""

    var body: some View {

            
//            ForEach(supplierData){ supplier in
//                Text(supplier.name)
//            }
            VStack{
                SearchBar(text: $searchTerm)
                List(productData.filter( { self.searchTerm.isEmpty ? true : $0.name.localizedCaseInsensitiveContains(self.searchTerm) })) {
                    product in
                    NavigationLink(destination: ProductDetail(product: product, warehouse: productWarehouse[0])) {
                        ProductRow(product: product,  warehouse: productWarehouse[0])
                    }
                }
            }

            .navigationBarTitle(Text("Productos"))
        
               
    }
}

struct ProductList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            ProductList()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}

