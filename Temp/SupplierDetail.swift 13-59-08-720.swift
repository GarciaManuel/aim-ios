//
//  SupplierDetail.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct SupplierDetail: View {
    var supplier: Supplier
    
    var body: some View {
        ScrollView{
            VStack {
               
               Rectangle()
                   .fill(Color.gray)
                   .frame(height: 300)
               Circle()
                   .fill(Color.blue)
                   .frame(height: 250)
                   .offset(x: 0, y: -130)
                   .padding(.bottom, -130)
               
               VStack(alignment: .leading) {
                    Text(supplier.name)
                        .font(.title)
                        .fixedSize(horizontal: false, vertical: true)
                   
                   HStack {
                       Text(supplier.email)
                           .underline()
                       Spacer()
                   }.fixedSize(horizontal: false, vertical: true)
                       .font(.subheadline)

                    HStack(alignment: .top) {
                        Text(supplier.address)
                            .font(.subheadline)
                        Spacer()
                    }.fixedSize(horizontal: false, vertical: true)

                    .padding(.bottom, 30)
                    .padding(.top, -10)
                
                   VStack {
                       HStack {
                           Text("Pago:")
                                   .bold()
                           Text(supplier.preferredPaymentMethod)
                               .font(.callout)
                           Spacer()
                       }.fixedSize(horizontal: false, vertical: true)
                           .padding(.bottom, 10)
                       HStack {
                           Text("RFC:")
                                   .bold()
                           Text(supplier.rfc.uppercased())
                               .font(.callout)
                           Spacer()
                       }.fixedSize(horizontal: false, vertical: true)
                       HStack {
                           Text("Cuenta Bancaria:")
                                   .bold()
                           Text(String(supplier.bankAccount))
                           Spacer()
                       }.fixedSize(horizontal: false, vertical: true)
                       HStack {
                           Text("Número de teléfono:")
                                   .bold()
                           Text(String(supplier.phoneNumber))
                               .font(.callout)
                           Spacer()
                       }.fixedSize(horizontal: false, vertical: true)
                   }
                   
                }
                .padding()

                Spacer()
            }
            .navigationBarTitle(Text(verbatim: supplier.name), displayMode: .inline)
        }
          
    }
}

struct SupplierDetail_Previews: PreviewProvider {
    static var previews: some View {
        SupplierDetail(supplier: supplierData[1])
    }
}
