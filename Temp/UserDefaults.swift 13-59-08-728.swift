//
//  UserDefaults.swift
//  AIM_2
//
//  Created by Manuel Garcia on 17/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI
import Combine
private var cancellables = [String : AnyCancellable]()

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
extension Published {
    init(wrappedValue defaultValue: Value, key: String) {
        let value = UserDefaults.standard.object(forKey: key) as? Value ?? defaultValue
        self.init(initialValue: value)
        cancellables[key] = projectedValue.sink { val in
            UserDefaults.standard.set(val, forKey: key)
        }
    }
}

class Settings: ObservableObject {
    @Published(key: "user") var user: String = ""
    @Published(key: "logged") var logged: Bool = false

}

