//
//  SupplierList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct SupplierList: View {
    @State private var searchTerm: String = ""

    var body: some View {

        NavigationView {
            
//            ForEach(supplierData){ supplier in
//                Text(supplier.name)
//            }
            VStack{
                SearchBar(text: $searchTerm)
                List(supplierData.filter( { self.searchTerm.isEmpty ? true : $0.name.localizedCaseInsensitiveContains(self.searchTerm) })) {
                    supplier in
                    NavigationLink(destination: SupplierDetail(supplier: supplier)) {
                        SupplierRow(supplier: supplier)
                    }
                }
            }

            .navigationBarTitle(Text("Proveedores"))
        }
               
    }
}

struct SupplierList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            SupplierList()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
