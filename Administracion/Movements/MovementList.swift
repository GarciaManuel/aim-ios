//
//  MovementList.swift
//  Administracion
//
//  Created by Manuel Garcia on 01/11/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Firebase

struct MovementList: View {
    @State private var searchTerm: String = ""
    @State private var inventoryMovements : [InventoryMovement] = []
    @State private var loaded = false
    @ObservedObject var Managers = ManagersObject()
    
    // Gets all warehouses
    @State private var warehouses : [Warehouse] = []
    @State private var warehouses_names : [String] = []
    
    @State private var movement_types : [MovementType] = []
    @State private var movement_Name_List : [String] = []
    
    //gets all suppliers
    @State private var suppliers : [Supplier] = []
    
//    @State private var movement_types : [MovementType] = []
//    var warehouses : Warehouse
//    @State private var warehouses : [Warehouse] = []
    
    @State private var addInvMovement = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    
    var body: some View {
        
        VStack {
//            navigationBarBackButtonHidden(false)
//            navigationBarHidden(true)
//            Button(
//                "Here is Detail View. Tap to go back.",
//                action: { self.presentationMode.wrappedValue.dismiss() }
//            )

            SearchBar(text: $searchTerm)
            
            List(inventoryMovements.filter( { self.searchTerm.isEmpty ? true : $0.product!.localizedCaseInsensitiveContains(self.searchTerm) })) { movement in
                NavigationLink(destination: MovementDetail(movement: movement, warehouses: self.warehouses, movement_types: self.movement_types, suppliers: self.suppliers)) {
                MovementRow(movement: movement)
                
                }}.onAppear { self.call_get_all_data() }
                .padding()
                .navigationBarTitle(Text("Movimientos"))
                
            .navigationBarItems(trailing:
                            Button(action: {
//                                self.addProduct.toggle()
                                print("Add is tapped!")
                                self.addInvMovement.toggle()
                                
                            }) {
//                                Text("Add")
                                Image(systemName: "plus")
                            }.sheet(isPresented: $addInvMovement){ AddMovement(warehouses: self.warehouses, movement_types: self.movement_types, movement_Name_List: self.movement_Name_List, warehouse_Name_List: self.warehouses_names, suppliers: self.suppliers)} //.onAppear(self.getalldata)
//                Button(action: {
//                                    self.addProduct.toggle()
//                                }) {
//                //                    Text("Add")
//                                    Image(systemName: "plus")
//                                }.sheet(isPresented: $addProduct) {
//                                    AddProductOptions(warehouse: self.warehouse)
//                                }
            )
        }
    }
    
    func call_get_all_data(){
            self.getInventoryMovements()

            self.getMovementTypes()
            self.getWarehouses()
            self.getSuppliers()
            
    //        self.loaded() is now done here in order to tell that everythin has been loaded
//            self.loaded = true
            
            print("FINISHED LOADING EVERYTHING!!!!!!")
    //            print("hello")
        }
    
    func getSuppliers(){

        if !loaded {
            Managers.mDataManager.getSuppliers().getDocuments{ (documentSnapshot, err) in

                if (err != nil) {
                    print("Error")
                    return
                }
                var supps: [Supplier] = []
                for document in (documentSnapshot?.documents)! {
                    print(document.data())
                    let supplier = Supplier(supplierData: document.data())
                    supps.append(supplier)
                }
                print("SUPPLIERS!!")
                print(supps)
                self.suppliers = supps
            }
//            self.loaded = true
        }
//
//
    }

        
    func getWarehouses(){
            
            if !loaded {
                Managers.mDataManager.getWarehouses().getDocuments{ (documentSnapshot, err) in

                    if (err != nil) {
                        print("Error")
                        return
                    }
                    var newWarehouses: [Warehouse] = []
//                        var newWHDocumentID : [String] = []
                    for document in (documentSnapshot?.documents)! {
                        print("Warehouse Docs \(document.data())")
                        let warehouse = Warehouse(warehouseData: document.data())
                        newWarehouses.append(warehouse)
//                            let warehouseDocument = document.documentID
//                            newWHDocumentID.append(warehouseDocument)
                    }
                    
                    for warehouse in newWarehouses{
//                        print("hello")
                        let warehouse_name = warehouse.name!
                        print(warehouse.name!)
                        self.warehouses_names.append(warehouse.name!)
                    }
//                    print(newWarehouses)
                    self.warehouses = newWarehouses
                }
//                    self.loaded = true
            }
        }
        
    func getMovementTypes(){
                
                if !loaded {
                    Managers.mDataManager.getAll_MovementTypes().getDocuments{ (documentSnapshot, err) in

                        if (err != nil) {
                            print("Error")
                            return
                        }
                        var newMovementTypes: [MovementType] = []
                        for document in (documentSnapshot?.documents)! {
                            print("MovementType Docs \(document.data())")
                            let movement_type = MovementType(movementTypeData: document.data())
//                            let movement_name =
                            newMovementTypes.append(movement_type)
                        }
                        print(newMovementTypes)
                        self.movement_types = newMovementTypes
                        
                        // assign to array names of movements
                        for movement in newMovementTypes{
                                print("hello")
                                let movement_name = movement.name!
                                print(movement.name!)
                                self.movement_Name_List.append(movement.name!)
                            }
//                        print("PRINT ALL MOVES")
//                        print(self.movement_Name_List)
                    }
//                    self.loaded = false
                    
                }
            }

        
    //    func getProducts(){
    //
    //        print("GETTING ALL INDEX OF WH PRODUCTS")
    //        print(warehouseProducts.count)
    //        print("Now all indexes")
    //        print(warehouseProducts)
    //
    //        if !loaded {
    //            for product in warehouseProducts{
    //                let amount = product["amount"] as! Float
    //                let productId = product["productId"] as! String
    //                print("PRODUCT ID")
    //                print(productId)
    //
    //                Managers.mDataManager.getProduct(productId: productId).getDocument{ (document, error) in
    //                    if let document = document, document.exists {
    //                        var newProduct =  Product(productData: document.data()!)
    //                        newProduct.amount = amount
    //                        self.products.append(newProduct)
    //                    } else {
    //                        print("Document does not exist")
    //                    }
    //                }
    //                print("PRODUCTS DATA :!!")
    //                print(self.products)
    //
    //            }
    //            self.loaded = true
    //        }
    //
    //    }

    
    func getInventoryMovements(){
//        if self.loaded == false {

            Managers.mDataManager.getInventoryMovements().getDocuments{ (documentSnapshot, err) in

                if (err != nil) {
                    print("Error")
                    return
                }
                var movements: [InventoryMovement] = []
                for document in (documentSnapshot?.documents)! {
                    print(document.data())
                    let movement = InventoryMovement(inventoryMovementData: document.data())
                    
                    print(movement.toStringId())
                    
                    movements.append(movement)
                }
                print(movements)
                self.inventoryMovements = movements
                
            }
//            self.loaded = true
//        }
    }
}

struct MovementList_Previews: PreviewProvider {
    static var previews: some View {
        MovementList()
    }
}
