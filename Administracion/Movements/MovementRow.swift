//
//  MovementRow.swift
//  Administracion
//
//  Created by Manuel Garcia on 31/10/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI

struct MovementRow: View {
    var movement: InventoryMovement
    var body: some View {
        HStack {
//            FirebaseImage(id: supplier.imageId!)
//               .frame(width: 50, height: 50)
//                .padding(.leading,10)
//
            VStack {
                HStack {
                    Text(movement.movementType!).underline()                        .fixedSize(horizontal: false, vertical: true)

                    Spacer()
                }
                
                HStack {
                    Text(movement.product!)                        .fixedSize(horizontal: false, vertical: true)

                    Spacer()
                    Text(String(movement.amount!))                        .fixedSize(horizontal: false, vertical: true)

                }.padding(.top, 10)
                HStack {
//                    Text("Surtido por: \(movement.supplier!)").font(.caption)                        .fixedSize(horizontal: false, vertical: true)

                    Spacer()
                }
                HStack {
                    Text("En: \(movement.warehouse!)").font(.caption)                        .fixedSize(horizontal: false, vertical: true)

                    Spacer()
                }
            }
            .fixedSize(horizontal: false, vertical: true)
            .padding(.leading,10)
            
        }    }
}

struct MovementRow_Previews: PreviewProvider {
    static var previews: some View {
        MovementRow(movement: InventoryMovement.getDefault())
    }
}
