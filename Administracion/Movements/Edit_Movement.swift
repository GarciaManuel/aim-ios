//
//  Edit_Movement.swift
//  Administracion
//
//  Created by Quique Posada on 11/25/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Firebase

struct Edit_Movement: View {
    var movement : InventoryMovement
    var warehouses : [Warehouse]
    var movement_types : [MovementType]
    var suppliers : [Supplier]
    
    @State private var new_name : String = ""
    @State private var new_description : String = ""
    @State private var new_entryunit : String = ""
    @State private var new_Product_to_find : String = ""
    @State private var new_Warehouse : String = ""
    @State private var new_Amount : String = ""
    
//    @State private var original_warehouse : Int = 0
    
    @State private var selected_warehouse = 0
    @State private var selected_supplier = 0
      
    @State private var selected_movement = 0
    
    @State private var selected_product : String = ""
    
    @State private var selected_warehouse_to_receive = 0
    
    @State private var products : [Product] = []
    
    @State private var product_exist_alert = false
    
    @ObservedObject var Managers = ManagersObject()
    
    let db = Firestore.firestore()
    
    let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 0.8)
      
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
        VStack{
            Text("Editar Movimiento")//.font(.title)
            
            NavigationView{
            Form{
                
                if(self.movement_types[selected_movement].name == "Inter-almacen" && movement.movementType == "Inter-almacen"){
                    Section{
                        //warehouse picker
                        Picker(selection: $selected_warehouse_to_receive, label: Text("Almacen a mover").font(.system(size: 12))) {
                            ForEach(0 ..< warehouses.count) {
                                Text(self.warehouses[$0].name!) // this seems to work
                                }
                            }.onAppear {
                            self.get_wh_Products() // on clicking a warehouse, get all warehouse products
    //                                print("Hello, view dissappeared")
                            }
                        }
                }
                else{
                     //movement type for now shall not be affected due to it having to change pretty mcuh everything
                    //section 2
                    Section{
                        //warehouse picker
                        Picker(selection: $selected_warehouse, label: Text("Almacen").font(.system(size: 12))) {
                            ForEach(0 ..< warehouses.count) {
                                Text(self.warehouses[$0].name!) // this seems to work
                                }
                            }.onAppear {
                            self.get_wh_Products() // on clicking a warehouse, get all warehouse products
    //                                print("Hello, view dissappeared")
                            }
                        }
                }
//                    //section3
//                    Section{
//                        //product picker
//                            Picker(selection: $selected_product, label: Text("Productos").font(.system(size: 12))) {
//                                ForEach(0 ..< (products.count)) {
//                                    Text(self.products[$0].name!)
//                                }.onTapGesture {
//                                    self.get_wh_Products()
//                                }
//                        }
//                        }
                
                //section4
                Section{
                    //supplier picker
                    Picker(selection: $selected_supplier, label: Text("Proveedores").font(.system(size: 12))) {
                        ForEach(0 ..< suppliers.count) {
                            Text(self.suppliers[$0].name!)
                        }
                    }
                }
                Section{
                    Picker(selection: $selected_product, label: Text("Productos en \(warehouses[selected_warehouse].name!)").font(.system(size: 12))) {
                        ForEach(self.products) { product in Text(product.name!).tag(product.name!) ;}
                    }
                }
                
                Section{
//                    HStack{
//                        Text("Producto")
//                        TextField("Producto : \(movement.product!)", text: $new_Product_to_find).background(lightGreyColor)
//                        .cornerRadius(5.0)
//                        .textFieldStyle(RoundedBorderTextFieldStyle())
//                        .padding()
//                    }
                    HStack{
                        Text("Cantidad")
                        TextField("Cantidad : \(String(movement.amount!))", text: $new_Amount).keyboardType(.numberPad).background(lightGreyColor)
                        .cornerRadius(5.0)
                        .padding()
                    }
                    HStack{
                        Text("Descripción")
                        TextField("Descripción : \(movement.description!)", text: $new_description).background(lightGreyColor)
                        .cornerRadius(5.0)
                        .padding()
                    }
//                    HStack{
//                        Text("Almacen")
//                        TextField("Almacen : \(movement.warehouse!)", text: $new_Warehouse)
//                    }
                }
                
                Section{
                    HStack{
                        Text("Tipo Movimiento")
                        Text(movement.movementType!).bold().underline()
                    }
                }
                
                
            }
            }
//                TextField("Producto : \(movement.product!)", text: $new_Product_to_find)
//                TextField("Cantidad : \(String(movement.amount!))", text: $new_Amount).keyboardType(.numberPad).padding()
//                TextField("Descripción : \(movement.description!)", text: $new_description)
//                TextField("Almacen : \(movement.warehouse!)", text: $new_Warehouse)
            
            
            if(!selected_product.isEmpty && !new_Amount.isEmpty && !new_description.isEmpty){
            Button(action: {
                        print("Edit tapped!")
                self.editData()
                        self.presentationMode.wrappedValue.dismiss()
                        
            //            NavigationLink(destination: ProductList(warehouse: self.warehouse))
                }){Text("Guardar Cambios")}.foregroundColor(Color.white)
                .padding()
                .background(Color.blue)
                .cornerRadius(5.0)
                
            }
//            if(product_exist_alert){
//    //                            Text("hello")
////                Alert(title: Text("El producto ingresado no existe en el almacen"), message: Text("No se realizaran los cambios pertinentes, haz un nuevo movimiento"), primaryButton: .destructive(Text("OK")) {
////                        self.presentationMode.wrappedValue.dismiss()
////                }, secondaryButton: .cancel())
////                Alert(message: Text("El producto ingresado no existe en el almacen"))
//            }
            
            
            }.navigationBarTitle("Editar Movimiento").onAppear{self.assignData()}
    }
    
    func assignData(){
        new_Product_to_find = movement.product!
        new_Amount = String(movement.amount!)
        new_description = movement.description!
        }
    
    func get_wh_Products(){
            print("Print all products of a warehouse")
    //        print(warehouses[selected_warehouse].products!)
            // YOU CAN DO THE SAME HERE FOR GETTING ALL PRODUCTS OF A WAREHOUSE
            products = []
            for product in warehouses[selected_warehouse].products!{
                        print("Product ID : \(product["productId"]!) with amount : \(product["amount"]!)")
                        let amount = product["amount"] as! Float
                        let productId = product["productId"] as! String
            //            print("PRODUCT ID")
            //            print(productId)

                        Managers.mDataManager.getProduct(productId: productId).getDocument{ (document, error) in
                            if let document = document, document.exists {
                                let newProduct =  Product(productData: document.data()!)
                                newProduct.amount = amount
                                self.products.append(newProduct)
                            } else {
                                print("Document does not exist")
                            }
                            
                        }
    //            print("PRODUCTS DATA :!!")
    //            print(self.products[product["productId"]])
            //            print("Product AMOUNT : \(product["amount"])")
                    }
            print("All products of the warehouse")
            print(self.products)
            
        }
    
    //edit  Movement Data
    func editData(){
        print("Edit tapped")
        
        var product_exists = false
        var product_id = ""
        var wh_product_current_amount : Float = 0
        var product_in_warehouse_index = 0
        var counter = 0
//        var product_createdAt : Timestamp?

        //check if product exists in warehouse
        for product in products{
            if(product.name == selected_product){
                product_exists = true
                product_id = product.id!
                product_in_warehouse_index = counter
                wh_product_current_amount = product.amount!
//                product_createdAt = product.createdAt
            }
            else{
                counter += 1
            }
        }
        if(!product_exists){
            product_exist_alert = true
        }
                
        // Edit Selected Product Document
        if(!new_Product_to_find.isEmpty && !new_description.isEmpty && !new_Amount.isEmpty && product_exists){
        
            let doc_to_edit_ref = db.collection("inventory_movements").document(movement.id!)
            print("INV MOVE ID \(movement.id!)")
            
            doc_to_edit_ref.updateData([
//                        "created_at": FieldValue.serverTimestamp(),
                        "description": new_description,
                        "amount": Float(self.new_Amount)!,
                        "movement_type": movement_types[selected_movement].name!,
                        "movement_type_id": db.document("movement_type/\(movement_types[selected_movement].id!)"),
                        "product": selected_product,
                        "product_id": db.document("product/\(product_id)"),
//                        "supplier": suppliername,
//                        "supplier_id": supplierID,
                        "warehouse": warehouses[selected_warehouse].name!,
                        "warehouse_id": db.document("warehouse/\(warehouses[selected_warehouse].id!)"),
                        "supplier": suppliers[selected_supplier].name!,
                        "supplier_id": db.document("supplier/\(suppliers[selected_supplier].id!)")
                    ]) { err in
                        if let err = err {
                            print("Error updating document: \(err)")
                        } else {
                            print("Document successfully updated")
                        }
                    }
            
            if(movement.movementType! == "Entrada"){ // add amount to the product specified
                                print("ENTRADA TYPE!!!!")
                                print("Adding...")
                                
//                                doc_to_edit_ref.updateData([
//                                    "product": selected_product,
//                                    "product_id": db.document("product/\(product_id)"),
//                                    "description": new_description,
//                                    "amount": Float(new_Amount),
//                        //            "movement_type": move,
//                                    "supplier": suppliers[selected_supplier].name!,
//                                    "supplier_id": db.document("supplier/\(suppliers[selected_supplier].id!)"),
//                                    "warehouse": warehouses[selected_warehouse].name!,
//                                    "warehouse_id": db.document("warehouse/\(warehouses[selected_warehouse].id!)"),
////                                    "updated_at": FieldValue.serverTimestamp()
//                                ]) { err in
//                                    if let err = err {
//                                        print("Error updating document: \(err)")
//                                    } else {
//                                        print("Document successfully updated")
//                                    }
//                                }
                                
            //                    let new_movement = ["product":product_name, "amount":Int(amount)!, "supplier":suppliers[selected_supplier].name!, "warehouse":warehouses[selected_warehouse].name!, "warehouse_id":db.document("warehouse/\(warehouses[selected_warehouse].id!)"), "description":description, "created_at":FieldValue.serverTimestamp(), "updated_at":FieldValue.serverTimestamp(), "product_id":db.document("product/\(product_id)"), "movement_type_id":db.document("movement_type/\(movement_types[selected_movement].id!)"), "id":ref!.documentID, "movement_type":movement_types[selected_movement].name!, "supplier_id":db.document("supplier/\(suppliers[selected_supplier].id!)")] as [String : Any]
                                
                                //now that document is added, update product in warehouse data by adding the new amount
                                
                                let new_amount = (Float(self.new_Amount)! + wh_product_current_amount)
                                
                                let product_wh_ref = db.collection("warehouse").document(warehouses[selected_warehouse].id!)
                                            // Set the "capital" field of the city 'DC'
                                            product_wh_ref.updateData([
                                                // update the object found at the specified index
                                                //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
                                                "products": FieldValue.arrayRemove([[
                                                // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
            //                                    "products.\(counter)":[
            //                                            "updated_at": FieldValue.serverTimestamp(),
            //                                            "created_at": FieldValue.serverTimestamp(),
                                                        "product": db.document("product/\(product_id)"),
                                                        "amount": wh_product_current_amount
            //                                    ]
                                                    ]])])
                                print("Array at POSITION \(counter) has been REMOVED")
                                
                                product_wh_ref.updateData([
                                    "products": FieldValue.arrayUnion([[
                                        "amount": new_amount,
                                        "product": db.document("product/\(product_id)"),
            //                            "created_at": product_createdAt!,
            //                            "updated_at": FieldValue.serverTimestamp()
                                        ]])
                                ])
                                print("The product at warehouse has been successfully UPDATED")
            //                    MovementList.append_movement_to_dict(new_movement)
            //                    warehouses[0].name = "HOLA Mundo"
                            }
            else if(movement.movementType! == "Salida"){ // rests amount to the product specified
                                // if amount reaches less than zero, then establish amount as 0
                                print("SALIDA!!!!")
                                print("Adding...")
//                                        doc_to_edit_ref.updateData([
//                                            "product": new_Product_to_find,
//                                            "product_id": db.document("product/\(product_id)"),
//                                            "description": new_description,
//                                            "amount": Float(new_Amount),
//                                //            "movement_type": move,
//                                            "supplier": suppliers[selected_supplier].name!,
//                                            "supplier_id": db.document("supplier/\(suppliers[selected_supplier].id!)"),
//                                            "warehouse": warehouses[selected_warehouse].name!,
//                                            "warehouse_id": db.document("warehouse/\(warehouses[selected_warehouse].id!)"),
//                                            "updated_at": FieldValue.serverTimestamp()
//                                        ]) { err in
//                                            if let err = err {
//                                                print("Error updating document: \(err)")
//                                            } else {
//                                                print("Document successfully updated")
//                                            }
//                                        }
//
                                //now that document is added, update product in warehouse data by adding the new amount
                                
                                var new_amount = ( wh_product_current_amount - Float(self.new_Amount)!)
                                if(new_amount < 0){
                                    new_amount = 0
                                }
                                
                                let product_wh_ref = db.collection("warehouse").document(warehouses[selected_warehouse].id!)
                                            // Set the "capital" field of the city 'DC'
                                            product_wh_ref.updateData([
                                                // update the object found at the specified index
                                                //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
                                                "products": FieldValue.arrayRemove([[
                                                // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
                //                                    "products.\(counter)":[
                //                                            "updated_at": FieldValue.serverTimestamp(),
                //                                            "created_at": FieldValue.serverTimestamp(),
                                                        "product": db.document("product/\(product_id)"),
                                                        "amount": wh_product_current_amount
                //                                    ]
                                                    ]])])
                                print("Array at POSITION \(counter) has been REMOVED")
                                
                                product_wh_ref.updateData([
                                    "products": FieldValue.arrayUnion([[
                                        "amount": new_amount,
                                        "product": db.document("product/\(product_id)"),
                //                            "created_at": product_createdAt!,
                //                            "updated_at": FieldValue.serverTimestamp()
                                        ]])
                                ])
                                print("The product at warehouse has been successfully UPDATED")

                            }
            else if(movement.movementType! == "Inter-almacen"){ // WTF does this do?
                                print("INTER ALMACEN")
                                
                                print("Adding...")
//                                        doc_to_edit_ref.updateData([
//                                            "product": new_Product_to_find,
//                                            "product_id": db.document("product/\(product_id)"),
//                                            "description": new_description,
//                                            "amount": Float(new_Amount),
//                                //            "movement_type": move,
//                                            "supplier": suppliers[selected_supplier].name!,
//                                            "supplier_id": db.document("supplier/\(suppliers[selected_supplier].id!)"),
//                                            "warehouse": warehouses[selected_warehouse].name!,
//                                            "warehouse_id": db.document("warehouse/\(warehouses[selected_warehouse].id!)"),
//                                            "updated_at": FieldValue.serverTimestamp()
//                                        ]) { err in
//                                            if let err = err {
//                                                print("Error updating document: \(err)")
//                                            } else {
//                                                print("Document successfully updated")
//                                            }
//                                        }
                                
                                //now that document is added, update product in warehouse data by adding the new amount
                                print("CURRENT AMOUNT !!! : \(wh_product_current_amount)")
                                var new_amount = ( wh_product_current_amount - Float(self.new_Amount)!)
                                print("Value of new amount!!! \(new_amount)")
                                

                                if(new_amount < 0){
                                    new_amount = 0
                                }
//                                else{
//                                    new_amount = new_amount
//                                }
                                print("Value of new amount!!!AFTER CONDITION \(new_amount)")
                                
                                // warehouse to recieve (Salida) product
                                let product_wh_tosend_ref = db.collection("warehouse").document(warehouses[selected_warehouse].id!)
                                            // Set the "capital" field of the city 'DC'
                                            product_wh_tosend_ref.updateData([
                                                // update the object found at the specified index
                                                //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
                                                "products": FieldValue.arrayRemove([[
                                                // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
                //                                    "products.\(counter)":[
                //                                            "updated_at": FieldValue.serverTimestamp(),
                //                                            "created_at": FieldValue.serverTimestamp(),
                                                        "product": db.document("product/\(product_id)"),
                                                        "amount": wh_product_current_amount
                //                                    ]
                                                    ]])])
                                print("Array at POSITION \(counter) has been REMOVED")
                                
                                product_wh_tosend_ref.updateData([
                                    "products": FieldValue.arrayUnion([[
                                        "amount": new_amount,
                                        "product": db.document("product/\(product_id)"),
                //                            "created_at": product_createdAt!,
                //                            "updated_at": FieldValue.serverTimestamp()
                                        ]])
                                ])
                                
                                
                                
//                                //warehouse to recieve(entrada) product
//                                var new_amount_recieved : Float = 0
//                                // if entered amount is greater than current amount, send current amount
//                                if(wh_product_current_amount < Float(self.new_Amount)!){
//                                    new_amount_recieved = wh_product_current_amount
//                                }
//                                else{
//                                    new_amount_recieved = Float(self.new_Amount)!
//                                }
//
//                                let product_wh_toreceive_ref = db.collection("warehouse").document(warehouses[selected_inter_warehouse].id!)
//                                                            // Set the "capital" field of the city 'DC'
//                                                            product_wh_toreceive_ref.updateData([
//                                                                // update the object found at the specified index
//                                                                //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
//                                                                "products": FieldValue.arrayRemove([[
//                                                                // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
//                                //                                    "products.\(counter)":[
//                                //                                            "updated_at": FieldValue.serverTimestamp(),
//                                //                                            "created_at": FieldValue.serverTimestamp(),
//                                                                        "product": db.document("product/\(product_id)"),
//                                                                        "amount": wh_product_current_amount
//                                //                                    ]
//                                                                    ]])])
//                                                print("Array at POSITION \(counter) has been REMOVED")
//
//                                                product_wh_toreceive_ref.updateData([
//                                                    "products": FieldValue.arrayUnion([[
//                                                        "amount": new_amount_recieved,
//                                                        "product": db.document("product/\(product_id)"),
//                                //                            "created_at": product_createdAt!,
//                                //                            "updated_at": FieldValue.serverTimestamp()
//                                                        ]])
//                                                ])
                                
                                
                                print("The product at warehouse has been successfully UPDATED")
                            }
                            else{
                                print("WTF?")
                            }
            
            
            
        }
        else{
            print("One field is empty, cancelling operation")
        }
        
    }
    
}

struct Edit_Movement_Previews: PreviewProvider {
    static var previews: some View {
        Edit_Movement(movement: InventoryMovement.getDefault(), warehouses: [Warehouse.getDefault()], movement_types: [MovementType.getDefault()], suppliers: [Supplier.getDefault()])
    }
}
