//
//  AddMovement.swift
//  Administracion
//
//  Created by Quique Posada on 11/23/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Firebase

struct AddMovement: View {
    @State private var loaded = false
    @ObservedObject var Managers = ManagersObject()
    
    @State private var selected_warehouse = 0
    @State private var selected_supplier = 0
    
    @State private var selected_product : String = "" // is a string instead o int because it would not work
    
    @State private var selected_movement = 0
    var warehouses : [Warehouse]
    var movement_types : [MovementType]
    var movement_Name_List : [String]
    
    var warehouse_Name_List : [String]
    
    var suppliers : [Supplier]
    
    @State private var selected_inter_warehouse = 0
    
    // stores all selected warehouse products
    @State private var products : [Product] = []

    @State private var product_name = ""
    @State private var amount = ""
    @State private var description = ""
    
    @State private var filled_form = false

    let db = Firestore.firestore()
    
    let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 0.8)
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        VStack{
            
//            Text("Agregar Movimiento de Inventario")
            Text("Agregar Movimiento")//.font(.title)
//            HStack{
            NavigationView{
                Form{
                    Section{
                        Picker(selection: $selected_movement, label: Text("Tipo de Movimiento").font(.system(size: 12))) {
                            ForEach(0 ..< movement_types.count) {
                                Text(self.movement_Name_List[$0])//.font(.system(size: 12))
                            }
//            }.onAppear{self.getMovementTypes()}
//            List
            
//            Text(movement_Name_list[0]).onAppear{self.getMovementTypes()}
//            Text("hello").onAppear{self.getWarehouses()}
//            }
//        }.onAppear{self.call_get_all_data()}
                            }
                        }
                    //section 2
                    Section{
                        //warehouse picker
                        Picker(selection: $selected_warehouse, label: Text("Almacen").font(.system(size: 12))) {
                            ForEach(0 ..< warehouse_Name_List.count) {
                                Text(self.warehouses[$0].name!) // this seems to work
                                }
                        }.onAppear{self.get_wh_Products()}
//                            .onTapGesture {
////                            self.loaded = false
//                            self.get_wh_Products() // on clicking a warehouse, get all warehouse products
//                            }
//                        }
                    
                    if(self.movement_types[selected_movement].name == "Inter-almacen"){
//                        Section{
                            //warehouse picker
                            Picker(selection: $selected_inter_warehouse, label: Text("Almacen a Mover Producto").font(.system(size: 12))) {
                                ForEach(0 ..< warehouses.count) {
                                    Text(self.warehouses[$0].name!) // this seems to work
                                    }
                                }
//                                .onAppear {
//                                    self.get_wh_Products() // on clicking a warehouse, get all warehouse products
//    //                                print("Hello, view dissappeared")
//                                }
//                        }
                        }
                    }
//                    section3
                    Section{
                        //product picker
//                            Picker(selection: $selected_product, label: Text("Productos").font(.system(size: 12))) {
//                                ForEach(0 ..< (products.count)) {_ in
//                                    Text(self.products[$0].name!)
//                                }
//                            }
                        Picker(selection: $selected_product, label: Text("Productos en \(warehouses[selected_warehouse].name!)").font(.system(size: 12))) {
                            ForEach(self.products) { product in Text(product.name!).tag(product.name!) ;}
                        }
//                        Text("Producto Escogido : \(selected_product)")
                        
//                        Text("\(products.count)")
//                            .onTapGesture {
//                            self.get_wh_Products()
//                        }
//                        for product in self.products{
////                            print(product.name!)
//                            Text("")
//                        }
                        
//                        List(self.products) { product in
//                            Text(product.name!) // works
//                        }
                        
                    }
//                    HStack{
//                        Text("\(products[0].name!)")
//                    }
                    
                        //section4
                    Section{
                        //supplier picker
                        Picker(selection: $selected_supplier, label: Text("Proveedores").font(.system(size: 12))) {
                            ForEach(0 ..< suppliers.count) {
                                Text(self.suppliers[$0].name!)
                            }
                        }
                    }
                    
                    
                    Section{
//                        TextField("Ingresa el Producto de \(warehouses[selected_warehouse].name!)", text: $product_name)
                        
                        TextField("Descripción de Movimiento", text: $description).padding()
                        .background(lightGreyColor)
                        .cornerRadius(5.0)
                        .padding()
                        
//                       check products of warehouse
//                        Button(action: {
//                            print("Products tapped!")
//                            self.test()
//                        }){Text("Buscar Productos de \(warehouse_Name_List[selected_warehouse])")}.foregroundColor(Color.white)
//                        .padding()
//                        .background(Color.blue)
//                        .cornerRadius(5.0)
                        
                        TextField("Cantidad", text: $amount).keyboardType(.numberPad).padding()
                        .background(lightGreyColor)
                        .cornerRadius(5.0)
                        .padding()
                        
                        }
                    }
                
                }

            
//            TextField("Ingresa el Producto", text: $product_name)
            // if form filled
            if(!selected_product.isEmpty && !self.amount.isEmpty && !description.isEmpty){
                Button(action: {
                print("Add tapped!")
                self.addData()
                self.presentationMode.wrappedValue.dismiss()
                }){Text("Agregar Movimiento \(movement_Name_List[selected_movement])")}.foregroundColor(Color.white)
                .padding()
                .background(Color.blue)
                .cornerRadius(5.0)
            }
        }//.onAppear{self.get_wh_Products()}
            .navigationBarTitle(Text("Agregar Movimiento").font(.title))
        
    }
    
    func test(){
        print("total amount of products is : \(products.count)" )
        print("Printing product names")
        for product in products{
            print(product.name!)
        }
//        ForEach(0 ..< (products.count)) {
////            Text(self.products[$0].name!)
////            print("hello")
//        }
        
        // try reload of products
//        ProductList.getProducts()
//        CONCLUSION: the problem is that loaded being false stops from being reloaded
//        meaning, change the val of loaded by putting a reload button on the corrspoing view of each one

    }
    
    func get_wh_Products(){
//        Text("Hello!!!!!")
        print("Print all products of a warehouse")
//        print(warehouses[selected_warehouse].products!)
        // YOU CAN DO THE SAME HERE FOR GETTING ALL PRODUCTS OF A WAREHOUSE
//        if !loaded {
            products = []
            for product in warehouses[selected_warehouse].products!{
                        print("Product ID : \(product["productId"]!) with amount : \(product["amount"]!)")
                        let amount = product["amount"] as! Float
                        let productId = product["productId"] as! String
            //            print("PRODUCT ID")
            //            print(productId)

                        Managers.mDataManager.getProduct(productId: productId).getDocument{ (document, error) in
                            if let document = document, document.exists {
                                let newProduct =  Product(productData: document.data()!)
                                newProduct.amount = amount
                                self.products.append(newProduct)
                                print(newProduct.name!)
                            } else {
                                print("Document does not exist")
                            }
                            
                        }
    //            print("PRODUCTS DATA :!!")
    //            print(self.products[product["productId"]])
            //            print("Product AMOUNT : \(product["amount"])")
                    }
//        }
//        self.loaded = true
//        print("All products of the warehouse")
//        print(self.products)
        
    }
    
    func addData(){
        // here is a test to print that we get the products of a warehouse
//        print(warehouses[selected_warehouse].name!)
//        get all products of warehouse
        
//        products = []
//        for product in warehouses[selected_warehouse].products!{
//            print("Product ID : \(product["productId"]!) with amount : \(product["amount"]!)")
//            let amount = product["amount"] as! Float
//            let productId = product["productId"] as! String
////            print("PRODUCT ID")
////            print(productId)
//
//            Managers.mDataManager.getProduct(productId: productId).getDocument{ (document, error) in
//                if let document = document, document.exists {
//                    let newProduct =  Product(productData: document.data()!)
//                    newProduct.amount = amount
//                    self.products.append(newProduct)
//                } else {
//                    print("Document does not exist")
//                }
//            }
        
//            print("PRODUCTS DATA :!!")
//            print(self.products)
//            print("Product AMOUNT : \(product["amount"])")
        var product_exists = false
        var product_id = ""
        var wh_product_current_amount : Float = 0
        var product_in_warehouse_index = 0
        var counter = 0
        var product_createdAt : Timestamp?

        //check if product exists in warehouse
        for product in products{
            if(product.name == selected_product){
                product_exists = true
                product_id = product.id!
                product_in_warehouse_index = counter
                wh_product_current_amount = product.amount!
                product_createdAt = product.createdAt
            }
            else{
                counter += 1
            }
        }
            
            // form is filled, add inventory movement
        if(!selected_product.isEmpty && !self.amount.isEmpty && !description.isEmpty && product_exists){
                if(movement_types[selected_movement].name! == "Entrada"){ // add amount to the product specified
                    print("ENTRADA TYPE!!!!")
                    print("Adding...")
                    
                    var ref: DocumentReference? = nil
                    ref = db.collection("inventory_movements").addDocument(data: [
                        "created_at": FieldValue.serverTimestamp(),
                        "description": description,
                        "amount": Float(self.amount)!,
                        "movement_type": movement_types[selected_movement].name!,
                        "movement_type_id": db.document("movement_type/\(movement_types[selected_movement].id!)"),
                        "product": selected_product,
                        "product_id": db.document("product/\(product_id)"),
//                        "supplier": suppliername,
//                        "supplier_id": supplierID,
                        "warehouse": warehouses[selected_warehouse].name!,
                        "warehouse_id": db.document("warehouse/\(warehouses[selected_warehouse].id!)"),
                        "supplier": suppliers[selected_supplier].name!,
                        "supplier_id": db.document("supplier/\(suppliers[selected_supplier].id!)")
                        
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                print("Product Document added with ID: \(ref!.documentID)")
                            }
                        }
                    db.collection("inventory_movements").document(ref!.documentID).setData(["id": ref!.documentID], merge: true) // merge document ID with document DATA
                    
//                    let new_movement = ["product":product_name, "amount":Int(amount)!, "supplier":suppliers[selected_supplier].name!, "warehouse":warehouses[selected_warehouse].name!, "warehouse_id":db.document("warehouse/\(warehouses[selected_warehouse].id!)"), "description":description, "created_at":FieldValue.serverTimestamp(), "updated_at":FieldValue.serverTimestamp(), "product_id":db.document("product/\(product_id)"), "movement_type_id":db.document("movement_type/\(movement_types[selected_movement].id!)"), "id":ref!.documentID, "movement_type":movement_types[selected_movement].name!, "supplier_id":db.document("supplier/\(suppliers[selected_supplier].id!)")] as [String : Any]
                    
                    //now that document is added, update product in warehouse data by adding the new amount
                    
                    let new_amount = (Float(self.amount)! + wh_product_current_amount)
                    
                    let product_wh_ref = db.collection("warehouse").document(warehouses[selected_warehouse].id!)
                                // Set the "capital" field of the city 'DC'
                                product_wh_ref.updateData([
                                    // update the object found at the specified index
                                    //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
                                    "products": FieldValue.arrayRemove([[
                                    // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
//                                    "products.\(counter)":[
//                                            "updated_at": FieldValue.serverTimestamp(),
//                                            "created_at": FieldValue.serverTimestamp(),
                                            "product": db.document("product/\(product_id)"),
                                            "amount": wh_product_current_amount
//                                    ]
                                        ]])])
                    print("Array at POSITION \(counter) has been REMOVED")
                    
                    product_wh_ref.updateData([
                        "products": FieldValue.arrayUnion([[
                            "amount": new_amount,
                            "product": db.document("product/\(product_id)"),
//                            "created_at": product_createdAt!,
//                            "updated_at": FieldValue.serverTimestamp()
                            ]])
                    ])
                    print("The product at warehouse has been successfully UPDATED")
//                    MovementList.append_movement_to_dict(new_movement)
//                    warehouses[0].name = "HOLA Mundo"
                }
                else if(movement_types[selected_movement].name! == "Salida"){ // rests amount to the product specified
                    // if amount reaches less than zero, then establish amount as 0
                    print("SALIDA!!!!")
                    print("Adding...")
                    var ref: DocumentReference? = nil
                    ref = db.collection("inventory_movements").addDocument(data: [
                        "created_at": FieldValue.serverTimestamp(),
                        "description": description,
                        "amount": Float(self.amount)!,
                        "movement_type": movement_types[selected_movement].name!,
                        "movement_type_id": db.document("movement_type/\(movement_types[selected_movement].id!)"),
                        "product": selected_product,
                        "product_id": db.document("product/\(product_id)"),
    //                        "supplier": suppliername,
    //                        "supplier_id": supplierID,
                        "warehouse": warehouses[selected_warehouse].name!,
                        "warehouse_id": db.document("warehouse/\(warehouses[selected_warehouse].id!)"),
                        "supplier": suppliers[selected_supplier].name!,
                        "supplier_id": db.document("supplier/\(suppliers[selected_supplier].id!)")
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                print("Product Document added with ID: \(ref!.documentID)")
                            }
                        }
                    db.collection("inventory_movements").document(ref!.documentID).setData(["id": ref!.documentID], merge: true) // merge document ID with document DATA
                    
                    //now that document is added, update product in warehouse data by adding the new amount
                    
                    var new_amount = ( wh_product_current_amount - Float(self.amount)!)
                    if(new_amount < 0){
                        new_amount = 0
                    }
                    
                    let product_wh_ref = db.collection("warehouse").document(warehouses[selected_warehouse].id!)
                                // Set the "capital" field of the city 'DC'
                                product_wh_ref.updateData([
                                    // update the object found at the specified index
                                    //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
                                    "products": FieldValue.arrayRemove([[
                                    // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
    //                                    "products.\(counter)":[
    //                                            "updated_at": FieldValue.serverTimestamp(),
    //                                            "created_at": FieldValue.serverTimestamp(),
                                            "product": db.document("product/\(product_id)"),
                                            "amount": wh_product_current_amount
    //                                    ]
                                        ]])])
                    print("Array at POSITION \(counter) has been REMOVED")
                    
                    product_wh_ref.updateData([
                        "products": FieldValue.arrayUnion([[
                            "amount": new_amount,
                            "product": db.document("product/\(product_id)"),
    //                            "created_at": product_createdAt!,
    //                            "updated_at": FieldValue.serverTimestamp()
                            ]])
                    ])
                    print("The product at warehouse has been successfully UPDATED")

                }
                else if(movement_types[selected_movement].name! == "Inter-almacen"){ // WTF does this do?
                    print("INTER ALMACEN")
                    
                    print("Adding...")
                    var ref: DocumentReference? = nil
                    ref = db.collection("inventory_movements").addDocument(data: [
                        "created_at": FieldValue.serverTimestamp(),
                        "description": description,
                        "amount": Float(self.amount)!,
                        "movement_type": movement_types[selected_movement].name!,
                        "movement_type_id": db.document("movement_type/\(movement_types[selected_movement].id!)"),
                        "product": selected_product,
                        "product_id": db.document("product/\(product_id)"),
    //                        "supplier": suppliername,
    //                        "supplier_id": supplierID,
                        "warehouse": warehouses[selected_warehouse].name!,
                        "warehouse_id": db.document("warehouse/\(warehouses[selected_inter_warehouse].id!)"),
                        "supplier": suppliers[selected_supplier].name!,
                        "supplier_id": db.document("supplier/\(suppliers[selected_supplier].id!)")
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                print("Product Document added with ID: \(ref!.documentID)")
                            }
                        }
                    db.collection("inventory_movements").document(ref!.documentID).setData(["id": ref!.documentID], merge: true) // merge document ID with document DATA
                    
                    //now that document is added, update product in warehouse data by adding the new amount
                    
                    var new_amount = ( wh_product_current_amount - Float(self.amount)!)
                    print("Value of new amount!!! \(new_amount)")
                    if(new_amount < 0){
                        new_amount = 0
                    }
//                    else{
//                        new_amount = Float(self.amount)!
//                    }
                    print("Value of new amount AFTERRRR CONDITION!!! \(new_amount)")

                    
                    // warehouse to send (Salida) product
                    let product_wh_tosend_ref = db.collection("warehouse").document(warehouses[selected_warehouse].id!)
                                // Set the "capital" field of the city 'DC'
                                product_wh_tosend_ref.updateData([
                                    // update the object found at the specified index
                                    //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
                                    "products": FieldValue.arrayRemove([[
                                    // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
    //                                    "products.\(counter)":[
    //                                            "updated_at": FieldValue.serverTimestamp(),
    //                                            "created_at": FieldValue.serverTimestamp(),
                                            "product": db.document("product/\(product_id)"),
                                            "amount": wh_product_current_amount
    //                                    ]
                                        ]])])
                    print("Array at POSITION \(counter) has been REMOVED")
                    
                    product_wh_tosend_ref.updateData([
                        "products": FieldValue.arrayUnion([[
                            "amount": new_amount,
                            "product": db.document("product/\(product_id)"),
    //                            "created_at": product_createdAt!,
    //                            "updated_at": FieldValue.serverTimestamp()
                            ]])
                    ])
                    
                    
                    
                    //warehouse to recieve(entrada) product
                    var new_amount_recieved : Float = 0
                    // if entered amount is greater than current amount, send current amount
                    if(wh_product_current_amount < Float(self.amount)!){
                        new_amount_recieved = wh_product_current_amount
                    }
                    else{
                        new_amount_recieved = Float(self.amount)!
                    }
                    print("NEW AMOUNT RECIEVED \(new_amount_recieved)")
                    
                    let product_wh_toreceive_ref = db.collection("warehouse").document(warehouses[selected_inter_warehouse].id!)
                                                // Set the "capital" field of the city 'DC'
                                                product_wh_toreceive_ref.updateData([
                                                    // update the object found at the specified index
                                                    //SOLO SE NECESITARIA UPDATED_AT, CREATED AT SE MANTIENE SOLO EN EL PRODUCTO!!!!
                                                    "products": FieldValue.arrayRemove([[
                                                    // AHORA VAMOS A REMOVER LA INSTANCIA DEL ARRAY Y AGREGAR LA "ACTUALIZADA"
                    //                                    "products.\(counter)":[
                    //                                            "updated_at": FieldValue.serverTimestamp(),
                    //                                            "created_at": FieldValue.serverTimestamp(),
                                                            "product": db.document("product/\(product_id)"),
                                                            "amount": wh_product_current_amount
                    //                                    ]
                                                        ]])])
                                    print("Array at POSITION \(counter) has been REMOVED")
                                    
                                    product_wh_toreceive_ref.updateData([
                                        "products": FieldValue.arrayUnion([[
                                            "amount": new_amount_recieved,
                                            "product": db.document("product/\(product_id)"),
                    //                            "created_at": product_createdAt!,
                    //                            "updated_at": FieldValue.serverTimestamp()
                                            ]])
                                    ])
                    
                    
                    print("The product at warehouse has been successfully UPDATED")
                }
                else{
                    print("WTF?")
                }
//                print(movement_types[selected_movement].name!)
                
            }
                // cant add incompleted inventory movement
            else{
                print("Cannot ADD Movement!!!!. Fill ALL Fields")
            }
        }
        
    }
    
    
//    func call_get_all_data(){
//        self.getMovementTypes()
//        self.getWarehouses()
//
////        self.loaded() is now done here in order to tell that everythin has been loaded
//        self.loaded = true
//
//        print("FINISHED LOADING EVERYTHING!!!!!!")
////            print("hello")
//    }
//
//        func getWarehouses(){
//
//                if !loaded {
//                    Managers.mDataManager.getWarehouses().getDocuments{ (documentSnapshot, err) in
//
//                        if (err != nil) {
//                            print("Error")
//                            return
//                        }
//                        var newWarehouses: [Warehouse] = []
////                        var newWHDocumentID : [String] = []
//                        for document in (documentSnapshot?.documents)! {
//                            print("Warehouse Docs \(document.data())")
//                            let warehouse = Warehouse(warehouseData: document.data())
//                            newWarehouses.append(warehouse)
////                            let warehouseDocument = document.documentID
////                            newWHDocumentID.append(warehouseDocument)
//                        }
//                        print(newWarehouses)
//                        self.warehouses = newWarehouses
//                    }
////                    self.loaded = true
//                }
//            }
//
//    func getMovementTypes(){
//
//                if !loaded {
//                    Managers.mDataManager.getAll_MovementTypes().getDocuments{ (documentSnapshot, err) in
//
//                        if (err != nil) {
//                            print("Error")
//                            return
//                        }
//                        var newMovementTypes: [MovementType] = []
//                        for document in (documentSnapshot?.documents)! {
//                            print("MovementType Docs \(document.data())")
//                            let movement_type = MovementType(movementTypeData: document.data())
////                            let movement_name =
//                            newMovementTypes.append(movement_type)
//                        }
//                        print(newMovementTypes)
//                        self.movement_types = newMovementTypes
//
//                        // assign to array names of movements
//                        for movement in newMovementTypes{
//                                print("hello")
//                                let movement_name = movement.name!
//                                print(movement.name!)
//                                self.movement_Name_list.append(movement.name!)
//                            }
//                        print("PRINT ALL MOVES")
//                        print(self.movement_Name_list)
//                    }
////                    self.loaded = false
//
//                }
//            }
//
//
////    func getProducts(){
////
////        print("GETTING ALL INDEX OF WH PRODUCTS")
////        print(warehouseProducts.count)
////        print("Now all indexes")
////        print(warehouseProducts)
////
////        if !loaded {
////            for product in warehouseProducts{
////                let amount = product["amount"] as! Float
////                let productId = product["productId"] as! String
////                print("PRODUCT ID")
////                print(productId)
////
////                Managers.mDataManager.getProduct(productId: productId).getDocument{ (document, error) in
////                    if let document = document, document.exists {
////                        var newProduct =  Product(productData: document.data()!)
////                        newProduct.amount = amount
////                        self.products.append(newProduct)
////                    } else {
////                        print("Document does not exist")
////                    }
////                }
////                print("PRODUCTS DATA :!!")
////                print(self.products)
////
////            }
////            self.loaded = true
////        }
////
////    }



struct AddMovement_Previews: PreviewProvider {
    static var previews: some View {
        AddMovement(warehouses: [Warehouse.getDefault(), Warehouse.getDefault()], movement_types: [MovementType.getDefault(), MovementType.getDefault()], movement_Name_List:[], warehouse_Name_List: [], suppliers: [Supplier.getDefault()])
    }
}
