//
//  MovementDetail.swift
//  Administracion
//
//  Created by Manuel Garcia on 01/11/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI

struct MovementDetail: View {
    var movement : InventoryMovement
    var warehouses : [Warehouse]
    var movement_types : [MovementType]
    var suppliers : [Supplier]
    
    //one for suppliers

    @State private var movementtype: String = ""
    @State var editMovement = false
    
    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello World!"/*@END_MENU_TOKEN@*/)
//        TextField("Tipo Movimiento", text: movement.movementType!)
//        movementtype = movement.movementType!
//        HStack {
        ScrollView{
            VStack{
                        Rectangle()
                                       .fill(Color.white)
                                       .frame(height: 150)
                VStack(alignment: .leading) {
                    Text(movement.product!)
                        .font(.title)
                        .fixedSize(horizontal: false, vertical: true)
                   
                   HStack {
                       Text("Tipo Movimiento")
                       Spacer()
                    Text(movement.movementType!)
                        .underline().bold()
                   }.fixedSize(horizontal: false, vertical: true)
                       .font(.subheadline)


                HStack(alignment: .top) {
                    Text("Descripción : ")
                    .underline()
                    Spacer()
                    Text(movement.description!)
                        .font(.subheadline)
                    Spacer()
                }.fixedSize(horizontal: false, vertical: true)

                    .padding(.bottom, 30)
                .padding(.top, 10)
                   VStack {
                       HStack {
                           Text("Cantidad:")
                                   .bold()
                        Text(String(movement.amount!))
                               .font(.callout)
                           Spacer()
                       }.fixedSize(horizontal: false, vertical: true)
                           .padding(.bottom, 10)
                       HStack {
                           Text("Surtido por :")
                                   .bold()
                        Text(movement.supplier!.uppercased())
                               .font(.callout)
                           Spacer()
                       }.fixedSize(horizontal: false, vertical: true)
                       HStack {
                           Text("En Almacen:")
                                   .bold()
                        Text(String(movement.warehouse!))
                           Spacer()
                       }.fixedSize(horizontal: false, vertical: true)
                   }
                   
                }.padding()
                
//                        VStack(spacing: 10){
//                            HStack {
//                                Text("Producto: ")
//                                Spacer()
//                                Text("\(movement.product!)").bold().fixedSize(horizontal: false, vertical: true)
//                            }
//
//                            Divider()
//
//                            HStack {
//                                Text("Tipo Movimiento: ")
//                                Spacer()
//                                Text("\(movement.movementType!)").underline().bold().fixedSize(horizontal: false, vertical: true)
//    //                            Spacer()
//                            }
//                            Divider()
//
//                            HStack{
//                            Text(("Cantidad: "))
//                            Spacer()
//                                Text("\(String(movement.amount!))").bold().italic().fixedSize(horizontal: false, vertical: true)
//                            }
//                            Divider()
//
////                        }.padding(.top, 10)
//                        HStack {
//                            Text("Surtido por:" ).fixedSize(horizontal: false, vertical: true)
//                            Spacer()
//                            Text("\(movement.supplier!)").bold().fixedSize(horizontal: false, vertical: true)
//                        }
//                            Divider()
//
//                        if(movement.movementType != "Inter-almacen"){
//                            HStack {
//                                Text("En Almacen: ")
//                                Spacer()
//                                Text("\(movement.warehouse!)").bold().underline().fixedSize(horizontal: false, vertical: true)
//                            }
//                            Divider()
//                        }
//                        else{
//                            HStack {
//                                Text("Movido al Almacen : ")
//                                Spacer()
//                                Text("\(movement.warehouse!)").bold().underline().fixedSize(horizontal: false, vertical: true)
//                            }
//                            Divider()
//                        }
//
//
//                        HStack {
//                            Text("Descripción: ")
//                            Spacer()
//                            Text("\(movement.description!)").fixedSize(horizontal: false, vertical: true)
//                        }
//                            }
                    }.fixedSize(horizontal: false, vertical: true).padding(.leading,10)
        }.navigationBarTitle(Text(verbatim: movement.product!), displayMode: .inline)
                .navigationBarItems(trailing:
                                Button(action: {
        //                            self.addProduct.toggle()
                                    print("Hello, edit tapped")
                                    self.editMovement.toggle()
                                }) {
                                    Text("Editar")
                                }.sheet(isPresented: $editMovement) {
                                    Edit_Movement(movement: self.movement, warehouses: self.warehouses, movement_types: self.movement_types, suppliers: self.suppliers)
                                }
        //                        .sheet(isPresented: $addProduct) {
        //                            EditProduct()
        //                        }

                                
                            )
                    
//                }
}

struct MovementDetail_Previews: PreviewProvider {
    static var previews: some View {
        MovementDetail(movement: InventoryMovement.getDefault(), warehouses: [Warehouse.getDefault()], movement_types: [MovementType.getDefault()], suppliers: [Supplier.getDefault()] )
    }
}
}
