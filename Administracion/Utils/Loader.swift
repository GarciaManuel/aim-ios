//
//  Loader.swift
//  Administracion
//
//  Created by Manuel Garcia on 31/10/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Combine
import FirebaseStorage

final class Loader : ObservableObject {
    let didChange = PassthroughSubject<Data?, Never>()
    var data: Data? = nil {
        didSet { didChange.send(data) }
    }

    init(_ id: String){
        // the path to the image
        let url = "images/\(id)"
        let storage = Storage.storage()
        let ref = storage.reference().child(url)
        ref.getData(maxSize: 1 * 1024 * 1024) { data, error in
            if let error = error {
                print("\(error)")
            }
            print("funciono")
            DispatchQueue.main.async {
                self.data = data
            }
        }
    }
}
