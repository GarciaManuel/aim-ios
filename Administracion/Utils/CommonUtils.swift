//
//  CommonUtils.swift
//  AIM
//
//  Created by Manuel Garcia on 9/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore

class CommonUtils {
    
    class func getIdFromReference(data: DocumentReference?) -> String {
        if(data != nil){
            return data!.documentID
        }
        return "None"
    }
    class func getController(withId id: String) -> UIViewController {
        return getStoryboard(withName: id).instantiateViewController(withIdentifier: id)
    }
    
    class func getStoryboard(withName name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle: nil)
    }
    
    class func setShadow(forViews views: [UIView]) {
        for v in views {
            v.clipsToBounds = false
            v.layer.shadowOpacity = 0.3
            v.layer.shadowOffset = CGSize(width: 0, height: 1)
            v.layer.shadowRadius = 3.0
            v.layer.shadowColor = UIColor.darkGray.cgColor
        }
    }
    
    class func setCorners(forViews views: [UIView]) {
        for v in views {
            v.layer.cornerRadius = 12
        }
    }
    
    class func setBottomCorners(forViews views: [UIView]) {
        for v in views {
            let path = UIBezierPath(roundedRect: v.bounds, byRoundingCorners: [.bottomRight, .bottomLeft], cornerRadii: CGSize(width: 12, height: 12))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            v.layer.mask = maskLayer
        }
    }
    
    class func setTopCorners(forViews views: [UIView]) {
        for v in views {
            let path = UIBezierPath(roundedRect: v.bounds, byRoundingCorners: [.topRight, .topLeft], cornerRadii: CGSize(width: 12, height: 12))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            v.layer.mask = maskLayer
        }
    }
    
    class func setHalfCorners(forViews views: [UIView]) {
        for v in views {
            v.clipsToBounds = false
            v.layer.cornerRadius = 6
        }
    }
    
    class func setBorderColor(forViews views: [UIView]) {
        for v in views {
            v.layer.borderColor = UIColor(red: 0.00, green: 0.54, blue: 0.64, alpha: 1.0).cgColor
            v.layer.borderWidth = 4
        }
    }
    
    class func setRoundedBackground(forView view: UIView) {
        view.layer.cornerRadius = view.bounds.height / 2
    }
    
    class func setRoundedBackground(forViews views: [UIView]) {
        for v in views {
            v.layer.cornerRadius = v.bounds.height / 2
        }
    }
    
    class func isEmailValid(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    class func isPhoneValid(_ phone: String) -> Bool {
        let p = getNumberCharactersOnly(phone)
        return p.count >= 10
    }
    
    class func getNumberCharactersOnly(_ phone: String) -> String {
        let set = CharacterSet(charactersIn: "0123456789")
        return phone.components(separatedBy: set.inverted).joined()
    }
    
}

extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}
