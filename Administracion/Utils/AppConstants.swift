//
//  AppConstants.swift
//  AIM
//
//  Created by Manuel Garcia on 9/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import UIKit

class AppConstants {
    
    public static let LOGGED_ANONYMOUS = "LOGGED_ANONYMOUS"
    
    public static let MONTHS: [String] = ["Enero",
                                          "Febrero",
                                          "Marzo",
                                          "Abril",
                                          "Mayo",
                                          "Junio",
                                          "Julio",
                                          "Agosto",
                                          "Septiembre",
                                          "Octubre",
                                          "Noviembre",
                                          "Diciembre"]
    
    public static let HOURS: [String] = ["12 AM",
                                         "1 AM",
                                         "2 AM",
                                         "3 AM",
                                         "4 AM",
                                         "5 AM",
                                         "6 AM",
                                         "7 AM",
                                         "8 AM",
                                         "9 AM",
                                         "10 AM",
                                         "11 AM",
                                         "12 PM",
                                         "1 PM",
                                         "2 PM",
                                         "3 PM",
                                         "4 PM",
                                         "5 PM",
                                         "6 PM",
                                         "7 PM",
                                         "8 PM",
                                         "9 PM",
                                         "10 PM",
                                         "11 PM"
    ]
    
    public static let stringColors : [String] = ["red",
                                                 "orange",
                                                 "yellow",
                                                 "green",
                                                 "fosfo",
                                                 "cyan",
                                                 "blue",
                                                 "strong_blue",
                                                 "purple",
                                                 "lavender",
                                                 "pink"]
    
    public static let objectColors : [UIColor] = [ColorUtils.red,
                                                  ColorUtils.orange,
                                                  ColorUtils.yellow,
                                                  ColorUtils.green,
                                                  ColorUtils.fosfo,
                                                  ColorUtils.cyan,
                                                  ColorUtils.blue,
                                                  ColorUtils.strong_blue,
                                                  ColorUtils.purple,
                                                  ColorUtils.lavender,
                                                  ColorUtils.pink]
    
}
