//
//  WarehouseRow.swift
//  AIM_2
//
//  Created by Manuel Garcia on 24/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct WarehouseRow: View {
    var warehouse: Warehouse
    
    var body: some View {
        HStack {
             FirebaseImage(id: warehouse.imageId!)
                .frame(width: 50, height: 50)
            Text(warehouse.name!)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
}

struct WarehouseRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            WarehouseRow(warehouse: Warehouse.getDefault())
            WarehouseRow(warehouse: Warehouse.getDefault())
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
