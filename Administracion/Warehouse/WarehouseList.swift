//
//  WarehouseList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 24/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct WarehouseList: View {
    @ObservedObject var store : Settings
    @State private var searchTerm: String = ""
    @State private var warehouses : [Warehouse] = []
    @State private var loaded = false
    @State private var showingAlert = false
    @ObservedObject var Managers = ManagersObject()
    
    @State private var warehousesDocsID : [String] = []
    
    @State private var selection_view = 0
    
    private var profileButton: some View {
        HStack{
//        TabView(selection: $selection_view){
//            Text("hello")
//        NavigationView{
            NavigationLink(destination: MovementList()) {
//                Text("Movimientos Inventario")
                Image("movement")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(minWidth: 25, maxWidth: 25, minHeight: 25, maxHeight: 25, alignment: .center)
                .clipped()
                .padding(.trailing, 20)
            }
//            }.tabItem {
//                VStack {
//
//                }
//            }//.tag(0)
        
//        NavigationView{
            NavigationLink(destination: SupplierList()) {
//               Text("Proveedores")
                Image("supplier")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(minWidth: 25, maxWidth: 25, minHeight: 25, maxHeight: 25, alignment: .center)
                .clipped()
                .padding(.trailing, 20)
            }
//        }//.tabItem{
//            VStack{
//
//            }
//            }//.tag(1)

            Button(action: {
                self.showingAlert = true

                print("Ir a content View")
            }) {
    //            Image(systemName: "person.crop.circle")
                Image("logout")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(minWidth: 25, maxWidth: 25, minHeight: 25, maxHeight: 25, alignment: .center)
                .clipped()       }
                .alert(isPresented:$showingAlert) {
                    Alert(title: Text("¿Quieres cerrar sesión?"), message: Text("Saldrás de tu cuenta."), primaryButton: .destructive(Text("Salir")) {
                            self.store.logged = false
                    }, secondaryButton: .cancel())
            }
//            }
        }
//            }
//        }
    }
    var body: some View {
        
        //body content
        NavigationView {
            VStack {
                SearchBar(text: $searchTerm)
                List(warehouses.filter( { self.searchTerm.isEmpty ? true : $0.name!.localizedCaseInsensitiveContains(self.searchTerm) })) { warehouse in
                    NavigationLink(destination: WarehouseDetail(warehouse: warehouse, warehouseID: self.warehousesDocsID[0])) {
                        // Forma temporal de pasar el id del documento
                        WarehouseRow(warehouse: warehouse)
                    }
                }.onAppear { self.getWarehouses() }
                .navigationBarTitle(Text("Almacenes")).navigationBarItems(trailing: profileButton )
                
            }

        }
               
    }
    
    func getWarehouses(){
        
        if !loaded {
            Managers.mDataManager.getWarehouses().getDocuments{ (documentSnapshot, err) in

                if (err != nil) {
                    print("Error")
                    return
                }
                var newWarehouses: [Warehouse] = []
                var newWHDocumentID : [String] = []
                for document in (documentSnapshot?.documents)! {
                    print("Warehouse Docs \(document.data())")
                    let warehouse = Warehouse(warehouseData: document.data())
                    newWarehouses.append(warehouse)
                    let warehouseDocument = document.documentID
//                    print("DocumentID : ")
//                    print(warehouseDocument)
                    newWHDocumentID.append(warehouseDocument)
                }
                print(newWarehouses)
//                print("DOCS ID")
//                print(newWHDocumentID)
                self.warehouses = newWarehouses
                self.warehousesDocsID = newWHDocumentID
                
            }
            self.loaded = true
        }

    }
    
}

struct WarehouseList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            WarehouseList(store: Settings())
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
