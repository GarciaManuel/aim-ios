//
//  WarehouseDetail.swift
//  AIM_2
//
//  Created by Manuel Garcia on 24/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct WarehouseDetail: View {
    var warehouse: Warehouse
    var warehouseID: String


    var body: some View {
        ScrollView {

        VStack {
            
            MapView(location: warehouse.location!)
                .frame(height: 300)
            

            FirebaseImage(id: warehouse.imageId!)       .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 4))
            .shadow(radius: 10)
                            .offset(x: 0, y: -130)
                            .padding(.bottom, -130)

            HStack {
                VStack(alignment: .leading) {
                    
                    Text(warehouse.name!)
                        .font(.title)
                        .fixedSize(horizontal: false, vertical: true)
                    
                    Text(warehouse.state!)
                        .font(.body)
                        
                    
                    HStack(alignment: .top) {
                        Text(warehouse.address!)
                            .font(.subheadline)
                            .fixedSize(horizontal: false, vertical: true)
                        Spacer()

                    }.padding(.top, 10)
                }
                .padding()
                

                NavigationLink(destination: UnitTypeList(),
                               label: { Text("Unidades")
                                .foregroundColor(Color.white)
                                    .padding()
                                .background(Color.orange)
                                    .cornerRadius(5.0)
                })
            


            }.padding()
            
            Spacer()
            
            HStack{

                NavigationLink(destination: ProductList(warehouseProducts: warehouse.products!, warehouse: warehouse),
                               label: { Text("Inventario existente")
                                .foregroundColor(Color.white)
                                    .padding()
                                    .background(Color.blue)
                                    .cornerRadius(5.0)
                })
            }

            Spacer()
        }
    }
        .navigationBarTitle(Text(verbatim: warehouse.name!), displayMode: .inline)
    
    }
}
struct WarehouseDetail_Previews: PreviewProvider {
    static var previews: some View {
        WarehouseDetail(warehouse: Warehouse.getDefault(), warehouseID: "")
    }
}
