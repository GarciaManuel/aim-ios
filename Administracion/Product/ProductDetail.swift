//
//  ProductDetail.swift
//  AIM_2
//
//  Created by Manuel Garcia on 26/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ProductDetail: View {
    var product : Product
    var warehouse : Warehouse
    var warehouseProducts : [[String : Any]]
    
    @State var editProduct = false
    
    @State private var qrtext = ""
    
    var lines : [Line] = []
    
    var unit_types : [UnitType] = []
    
    
    var body: some View {
        ScrollView{
            VStack {
//                Text(product.id!)
                Rectangle()
                .fill(Color.gray)
                .frame(height: 300)
                
                FirebaseImage(id: product.imageId!)       .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                .shadow(radius: 10)
                                .offset(x: 0, y: -130)
                                .padding(.bottom, -130)
                
                VStack(alignment: .leading) {
                    HStack {
                        Text(product.name!)
                            .font(.title)
                        Spacer()
                        Text(String(product.amount!))
                            .font(.largeTitle)
                            .bold()
                            .fixedSize(horizontal: false, vertical: true)

                    }
                    
                    HStack(alignment: .top) {
                        Text(product.line!)
                            .fixedSize(horizontal: false, vertical: true)
                            .font(.subheadline)
                        Spacer()
                        Text(product.entryUnit!)
                            .fixedSize(horizontal: false, vertical: true)
                            .font(.subheadline)
                    }
                    HStack{
                        Text(product.description!)
                            .fixedSize(horizontal: false, vertical: true)
                        .padding(.top, 50)

                    }
                    HStack{
                        Text("Se empaca en \(product.outUnit!)")
                        .lineLimit(nil)
                            .fixedSize(horizontal: false, vertical: true)
                            .font(.subheadline)

//                        .padding(.top, 50)

                    }
                    //qr code to register product in other wh
                    HStack(alignment: .center){
                        if (qrtext != ""){
                            Image(uiImage: UIImage(data: createQR(qrdata: self.qrtext))!).resizable().frame(width: 150, height: 150)
                        }
                        }.onAppear{self.fill_qr_dict()}

                  
                    
                }.padding()
            }.fixedSize(horizontal: false, vertical: true)
            
        }.navigationBarTitle(Text(verbatim: product.name!), displayMode: .inline)
        .navigationBarItems(trailing:
                        Button(action: {
//                            self.addProduct.toggle()
                            print("Hello, edit tapped")
                            self.editProduct.toggle()
                        }) {
                            Text("Editar")
                        }.sheet(isPresented: $editProduct) {
                            Edit_Selected_Product(product: self.product, warehouse: self.warehouse, warehouseProducts: self.warehouseProducts)
                        }
//                        .sheet(isPresented: $addProduct) {
//                            EditProduct()
//                        }

                        
                    )
    }
    func fill_qr_dict(){
        qrtext = "[{\"name\": \"\(product.name!)\", \"description\": \"\(product.description!)\", \"entry_unit\": \(product.entryUnit!), \"out_unit\": \(product.outUnit!), \"line\": \"\(product.line!)\", \"imageId\": \"\(product.imageId!)\"}]"
        // maybe you could call interalmacen once the QR is found
        //point is to open camera, and taking the warehouse in view, registers the product and adds it to that warehouse
        
        //convert string to json
        let data = qrtext.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
            {
               print(jsonArray) // use the json here
            } else {
                print("this did not work at all")
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    //creates product QR to register other products
    func createQR(qrdata : String)->Data{ // returns byte buffer in memory
    //        let format = VisionBarcodeFormat.all
    //        let barcodeOptions = VisionBarcodeDetectorOptions(formats: format)
    //
    //        let vision = Vision.vision()
    //        let barcodeDetector = vision.barcodeDetector(options: barcodeOptions)
            let filter = CIFilter(name: "CIQRCodeGenerator") // produces an image by manipulating input
            let data = qrdata.data(using: .utf8, allowLossyConversion: false)
            filter?.setValue(data, forKey: "inputMessage")
            let image = filter?.outputImage
            let uiimage = UIImage(ciImage: image!)
            return uiimage.pngData()!
            
    //        let image = VisionImage(image: uiImage)
            
        }
}

struct ProductDetail_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetail(product: Product.getDefault(), warehouse: Warehouse.getDefault(), warehouseProducts: Warehouse.getDefault().products!, lines: [Line.getDefault()], unit_types: [UnitType.getDefault()])
    }
}

