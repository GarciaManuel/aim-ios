//
//  AddProductOptions.swift
//  Administracion
//
//  Created by Quique Posada on 11/9/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Firebase
//import Fire

struct AddProductOptions: View {
    @State private var productname: String = ""
    @State private var product_description: String = ""
    @State private var quantity : String = ""
    @State private var line_name : String = ""
    @State private var entryunit : String = ""
    @State private var exitunit : String = ""
    @State private var imageid : String = "box.png" //hard-coded
    
    var warehouse : Warehouse
    
    var lines : [Line]
    
    var unit_types : [UnitType]
    
    @State private var loaded = false
    @State private var showcameraview = false
    
    @State private var selected_entry_unit = 0
    @State private var selected_out_unit = 0
    @State private var selected_line = 0
    
//    @State private var lines : [Line] = []
//    var existing_lines = [String]()
    // for dropdown
    @State private var selection = 1
//    var imagePicker: UIImagePickerController!
    
    let db = Firestore.firestore()
    
    let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 0.8)
    
    @ObservedObject var Managers = ManagersObject()
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>


    
    
//    @State private var current_lines : [Line] = []
//    var existinglines : [[String: Any]]
//    @ObservedObject var Managers = ManagersObject()

    
//    private var top: some View{
//        Text("Agregar Nuevo Producto").font(.system(size: 32))
//    }
    
    var body: some View {
        
        
        VStack{
            Text("Registrar Producto")//.font(.title)
//            HStack{
//                Text("hello")
//            }
            
            // Add from supplier table and from product table
//            Text("Agregar Nuevo Producto").font(.system(size: 32))
            
//            List{
//                ForEach()
//            }

//            Button(action: {
//                    print("Open Camera tapped!")
//                //Rest of actions here
//
//                            }) {
////                Image(systemName: "camera.fill")
//                Text("Abrir Camera")
////            }.sheet(isPresented: self.$showcameraview, content: CameraView
//                }
//            .background(Color.blue).foregroundColor(Color.white).padding()
            
            
            
            //Fetching lines
//            List {
//                ForEach(self.Line.name) { line_name in
//                        Text(line_name)
//                    }
//                }
//            }
            
//            self.getLines()
//            Text(lines.filter()).onAppear{self.getLine_ver3()}
//            List(lines).onAppear { self.getInventoryMovements() }
//                .padding()
//                .navigationBarTitle(Text("Movimientos"))
//            Text(lines.name)
//            lines = self.getLine_ver3()
            NavigationView{
                Form{
                    TextField("Nombre de Producto", text: $productname).background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding()
                    //.padding()
                    TextField("Descripción", text: $product_description).background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding()//.padding()
                    TextField("Cantidad", text: $quantity).keyboardType(.numberPad).background(lightGreyColor)
                    .cornerRadius(5.0)
                    .padding()//.padding()
        //            Picker(selection: $selection, label: Text("Linea").padding()) {
        //                Text("Químicos").tag(1) // hard-code
        //                Text("Instrumentelle Analytik").tag(2)
        //            }

                    Section{
                        Picker(selection: $selected_line, label: Text("Linea").font(.system(size: 12))) {
                        ForEach(0 ..< lines.count) {
                            Text(self.lines[$0].name!) // this seems to work
                            }
                        }
                        
                        Picker(selection: $selected_entry_unit, label: Text("Unidad Entrada").font(.system(size: 12))) {
                        ForEach(0 ..< unit_types.count) {
                            Text(self.unit_types[$0].name!) // this seems to work
                            }
                        }
                        
                        Picker(selection: $selected_out_unit, label: Text("Unidad Salida").font(.system(size: 12))) {
                        ForEach(0 ..< unit_types.count) {
                            Text(self.unit_types[$0].name!) // this seems to work
                            }
                        }
                    }
//                    TextField("Linea", text: $line_name).padding()
                    
//                    TextField("Unidad Entrada", text: $entryunit).padding()
//                    TextField("Unidad Salida", text: $exitunit).padding()
                    
                    
        //            Text("Almacen :  \(warehouse.name!)")
                    
        //            TextField()
        //            TextField()
        //            TextField()
        //            Text("Product to ADD, \(productname)!")
                    
//                    Spacer()
                    }
            }
            HStack{
                Text("Almacen : ")
    //                Spacer()
    //                Divider()
                Text(warehouse.name!).font(.system(size : 24))
    //                Text(warehouse.id!)
            }
            .lineSpacing(10)
            
            if(!productname.isEmpty && !product_description.isEmpty && !quantity.isEmpty){
            Button(action: {
                print("Add tapped!")
//                self.warehouse.name = "Prueba de WH"
                self.addData()
                self.presentationMode.wrappedValue.dismiss()
                }){Text("Agregar a \(warehouse.name!)")}.foregroundColor(Color.white)
                .padding()
                .background(Color.blue)
                .cornerRadius(5.0)
            }
        }
                

//        lines = getLines()
        .navigationBarTitle(Text("Registar Nuevo Producto").font(.title))
    }
    
    // Get all lines for user to choose
//    func getLines(){
//       self.db.collection("line").getDocuments { (snapshot, err) in
//           if let err = err {
//               print("Error getting documents from collection: \(err)")
//           } else {
//               for document in snapshot!.documents {
//                  let docId = document.documentID
//                  let line_description = document.get("description") as! String
//                  let line_name = document.get("name") as! String
//                  print(docId, line_description, line_name)
//               }
//           }
//        }
//    }
    
    
    // Now it is not necessary but good to know how to do it
    
//    func getLine_ver3(){
//            if !loaded {
//                Managers.mDataManager.getLines().getDocuments{ (documentSnapshot, err) in
//                    print(documentSnapshot?.documents)
//
//                    if (err != nil) {
//                        print("Error")
//                        return
//                    }
//                    var newLines: [Line] = []
//                    var LineNames:[String] = []
//                    for document in (documentSnapshot?.documents)! {
//                        print("Line Docs \(document.data())")
//                        let line = Line(lineData: document.data())
////                        let name =
//                        newLines.append(line)
//                    }
////                    print("HELLOO!")
//                    print(newLines)
//                    self.lines = newLines
//
//                }
//                self.loaded = true
//            }
//
//    }
    
    func addData(){
//        let line_add = db.collection("product")
        print("ADD FUNC CALLED!")
        print(productname)
        if(!productname.isEmpty && !product_description.isEmpty && !quantity.isEmpty){
            print("No empty string in all fields! Good")
            print("Adding...")
//            var id_to_add = ""
            var ref: DocumentReference? = nil
//            ref = db.collection("product").document()
//            id_to_add = ref!.documentID
            ref = db.collection("product").addDocument(data: [
                "created_at": FieldValue.serverTimestamp(),
//                "id": ref!.documentID,
//                "id": id_to_add,
                "description": product_description,
                "name": productname,
                "entry_unit": unit_types[selected_entry_unit].name!,
                "out_unit": unit_types[selected_out_unit].name!,
                "line": lines[selected_line].name,
                "imageId": imageid,
                "updated_at": FieldValue.serverTimestamp()
                //"created_at": firebase.database.ServerValue.TIMESTAMP,
                //"updated_at": firebase.database.ServerValue.TIMESTAMP
            ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Product Document added with ID: \(ref!.documentID)")
//                    "id": ref!.documentID
//                    print("Product Document added with ID: \(id_to_add)") // maybe this way it'll work (IT DID NOT)
                }
            }
            db.collection("product").document(ref!.documentID).setData(["id": ref!.documentID], merge: true) // merge document ID with document DATA
            
            // Get a reference to the storage service using the default Firebase App
            //let storage = Storage.storage()

            // Create a storage reference from our storage service
            //let storageRef = storage.reference()
            //let imageref = storageRef.child("images/mountains.jpg")
            
            //to add reference to products in warehouse
            var ref2: DocumentReference? = nil
//            let get_id_new_product = db.document("product").documentID
            
            let product_wh_ref = db.collection("warehouse").document(warehouse.id!)
            
            // never got to use this way of inserting a dict to the database
//            let product_dict = [
//                "amount" : Int(quantity)!,
//                "created_at" : FieldValue.serverTimestamp(),
//                "product" : db.document("product/\(ref!.documentID)"),
//                "updated_at" : FieldValue.serverTimestamp()
//                ] as [String : Any]
//
//            product_wh_ref.updateData("products" : FieldValue.arrayUnion({
//                "hello"
//                }))
            
            // Set the "capital" field of the city 'DC'
            product_wh_ref.updateData([
//                "products": FieldValue.arrayUnion([product_dict])
                "products": FieldValue.arrayUnion([[
                    "amount" : Int(quantity)!,
//                    "created_at": FieldValue.serverTimestamp(),
                    "product": db.document("product/\(ref!.documentID)")
//                    "updated_at": FieldValue.serverTimestamp()
                    ]])
                
//                    "amount": quantity,
//                    product_dict
                    
//                ]
//                    "amount": quantity
                    
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
            
            // Automatically add a new region to the "regions" array field.
//            product_wh_ref.updateData([
//                "products": FieldValue.arrayUnion([
////                    "greater_virginia"
//                    product_dict
////                    quantity,
////                    FieldValue.serverTimestamp(), // created_at
////                    FieldValue.serverTimestamp(), // updated_at
////                    ref?.documentID // product reference ID
//                ])
//            ])
//            product_wh_ref.updateData(<#T##fields: [AnyHashable : Any]##[AnyHashable : Any]#>)

//            ref2 = db.collection("warehouse").setValue(<#T##value: Any?##Any?#>, forKey: <#T##String#>)
//            (data: [
//                            "ref": CommonUtils.getIdFromReference(data: ref),// get product reference from previously added product
//                            "description": product_description,
//                            "name": productname,
//                            "entry_unit": entryunit,
//                            "out_unit": exitunit,
//                            "line": line_name,
//                            "imageId": imageid,
//                            "quantity": quantity
//            //                "created_at": firebase.database.ServerValue.TIMESTAMP,
//            //                "updated_at": firebase.database.ServerValue.TIMESTAMP
//                        ]) { err in
//                            if let err = err {
//                                print("Error adding document: \(err)")
//                            } else {
//                                print("Product Document added with ID: \(ref2!.documentID)")
//                            }
//                        }
        }
        else{
            print("One Field is empty! Not doing anything!!!!")
            print("print warehouse id")
//            let docToDelete = db.collection("warehouse").where('content', '==', 'CONTENT_GOES_HERE')
            let doc_warehouse = db.collection("warehouse").whereField("name", isEqualTo: warehouse.name!)
//            doc_warehouse.getDocuments(completion: DocumentSnapshot)
            print(doc_warehouse)
            print("Test 2 : ")
            print(warehouse.id!)
            print("Checking INFO of Product")
            print("Quantity: !!!")
            print(Int(quantity) ?? 0)
            
            print("FINAL TEST, getting DOCUMENT ID TO ADD !!!")
            var ref: DocumentReference? = nil
            ref = db.collection("product").document()
            print(ref!.documentID)
//            var product_dict = [
//            "amount" : quantity,
//            "created_at" : FieldValue.serverTimestamp(),
//            "product" : "ref",
//            "updated_at" : FieldValue.serverTimestamp()
//            ] as [String : Any]
//            print(product_dict)
//            print(db.collection("warehouse").document(doc_warehouse))
//            print(doc_warehouse.getDocuments(completion: <#T##FIRQuerySnapshotBlock##FIRQuerySnapshotBlock##(QuerySnapshot?, Error?) -> Void#>))
//            let getId_warehouse = db.collection("warehouse").document()
//            let getId_warehouse = db.document("warehouse").documentID
            //Gets the next auto generated ID
//            print(getId_warehouse.documentID)
//            print(warehouse.id)
//            print(doc)
//            print(CommonUtils.getIdFromReference(data: (warehouse as! DocumentReference)))
        }
        
    }
    
    //VERSION 2
//    func getLines_2(){
//
//        if !loaded {
//            for line in existing_lines{
//                let name = line["name"] as! String
//                let productId = line["lineId"] as! String
//
//                Managers.mDataManager.getLineById(lineId: productId).getDocument{ (document, error) in
//                    if let document = document, document.exists {
//                        var newProduct =  Line(lineData: document.data()!)
//                        newProduct.name = name
//                        self.lines.append(newProduct)
//                    } else {
//                        print("Document does not exist")
//                    }
//                }
//
//            }
//            self.loaded = true
//        }
//
//    }
    
}

struct CameraView: UIViewControllerRepresentable {

@Binding var showCameraView: Bool
@Binding var pickedImage: Image

func makeCoordinator() -> CameraView.Coordinator {
    Coordinator(self)
}

func makeUIViewController(context: UIViewControllerRepresentableContext<CameraView>) -> UIViewController {
    let cameraViewController = UIImagePickerController()
    cameraViewController.delegate = context.coordinator
    cameraViewController.sourceType = .camera
    cameraViewController.allowsEditing = false
    return cameraViewController
}

func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<CameraView>) {

}

class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var parent: CameraView

    init(_ cameraView: CameraView) {
        self.parent = cameraView
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let uiImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        parent.pickedImage = Image(uiImage: uiImage)
        parent.showCameraView = false
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        parent.showCameraView = false
    }
}

}

struct AddProductOptions_Previews: PreviewProvider {
    static var previews: some View {
//        AddProductOptions(existing_lines: Line.getDefault().id!)
        AddProductOptions(warehouse: Warehouse.getDefault(), lines: [Line.getDefault()], unit_types: [UnitType.getDefault()])
//        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
//            AddProductOptions(existing_lines: Line.getDefault().id!)
//            .previewDevice(PreviewDevice(rawValue: deviceName))
//            .previewDisplayName(deviceName)
    }
}
