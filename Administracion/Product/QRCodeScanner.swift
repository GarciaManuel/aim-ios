//
//  BarcodeCamera.swift
//  Administracion
//
//  Created by Quique Posada on 11/9/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Firebase

struct QRCodeScanner : View{
    
    //Aqui ira el contenido de abrir la camerapara el registro de un proucto al ware house de donde haya sido abierto
    
    @State private var qrtext = "[\"product\": \"perro\"]"
//    let format = VisionBarcodeFormat.all
//    lazy var barcodeOptions = VisionBarcodeDetectorOptions(formats: format)
//
//    lazy var vision = Vision.vision()
//    lazy var barcodeDetector = vision.barcodeDetector(options: barcodeOptions)
    
    var body: some View {
//        Text("Hellow")
        VStack{
            TextField("Enter Data of QR:", text: $qrtext)
            if (qrtext != ""){
    //            Text("hellowro")
                Image(uiImage: UIImage(data: createQR(qrdata: self.qrtext))!).resizable().frame(width: 150, height: 150)
            }
        }
        
    }
    
    func createQR(qrdata : String)->Data{ // returns byte buffer in memory
//        let format = VisionBarcodeFormat.all
//        let barcodeOptions = VisionBarcodeDetectorOptions(formats: format)
//
//        let vision = Vision.vision()
//        let barcodeDetector = vision.barcodeDetector(options: barcodeOptions)
        let filter = CIFilter(name: "CIQRCodeGenerator") // produces an image by manipulating input
        let data = qrdata.data(using: .ascii, allowLossyConversion: false)
        filter?.setValue(data, forKey: "inputMessage")
        let image = filter?.outputImage
        let uiimage = UIImage(ciImage: image!)
        return uiimage.pngData()!
        
//        let image = VisionImage(image: uiImage)
        
    }
}


//struct QRCodeScan: UIViewControllerRepresentable {
//
//    func makeCoordinator() -> Coordinator {
//        Coordinator(self)
//    }
//
//    func makeUIViewController(context: Context) -> QRCodeScanVC {
//        let vc = QRCodeScanVC()
//        vc.delegate = context.coordinator
//        return vc
//    }
//
//    func updateUIViewController(_ vc: QRCodeScanVC, context: Context) {
//    }
//
//    class Coordinator: NSObject, QRCodeScannerDelegate {
//
//        func codeDidFind(_ code: String) {
//            print(code)
//        }
//
//        var parent: QRCodeScan
//
//        init(_ parent: QRCodeScan) {
//            self.parent = parent
//        }
//    }
//}
//
//struct QRCodeScan_Previews: PreviewProvider {
//    static var previews: some View {
//        QRCodeScan()
//    }
//}


//class ImagePickerCoordinator: NSObject, UINavigationControllerDelegate, UIImagePickerController{
//    func
//}
//
//struct BarcodeCamera: UIViewControllerRepresentable {
//
//    func updateUIViewController(_ uiViewController: BarcodeCamera.UIViewControllerType, context: UIViewControllerRepresentableContext<BarcodeCamera>) {
//        <#code#>
//    }
//    func makeCoordinator() -> BarcodeCamera.Coordinator {
//        <#code#>
//    }
//    func makeUIViewController(context: UIViewControllerRepresentableContext<BarcodeCamera>) -> UIImagePickerController {
//        let picker = UIImagePickerController()
//        picker.delegate = context.coordinator
//        return picker
//    }
//}

//struct BarcodeCamera: View {
//    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
//    }
//}
//
//struct BarcodeCamera_Previews: PreviewProvider {
//    static var previews: some View {
//        BarcodeCamera()
//    }
//}
