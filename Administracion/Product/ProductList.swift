//
//  ProductList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 03/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ProductList: View {
    @State private var searchTerm: String = ""
    @State private var products : [Product] = []
    @State private var loaded = false
    var warehouseProducts: [[String: Any]]
    @ObservedObject var Managers = ManagersObject()
    
//    For warehouse name
    var warehouse: Warehouse
    
    @State private var lines : [Line] = []
    
    @State private var unit_types : [UnitType] = []

    
    @State var addProduct = false
    
    @State var cameraProduct = false
    
    @State private var addInvMovement = false
    
//    var headerButton: some View{
//                HStack{
//
//        }
//    }
    
    var body: some View {

            
//            ForEach(supplierData){ supplier in
//                Text(supplier.name)
//            }

        
            VStack{
                SearchBar(text: $searchTerm)
                List(products.filter( { self.searchTerm.isEmpty ? true :$0.name!.localizedCaseInsensitiveContains(self.searchTerm)})) {
                    product in
                    NavigationLink(destination: ProductDetail(product: product, warehouse: self.warehouse, warehouseProducts: self.warehouseProducts, lines: self.lines, unit_types: self.unit_types)) {
                        ProductRow(product: product)
                    }
                }.onAppear { self.call_get_all_data() }
                    .onDisappear{self.products=[]}
                Text("\(products.count)")
            }

            .navigationBarTitle(Text("Productos"))
            .navigationBarItems(trailing:
                
//                Button(action: {
//                    print("Add product tapped!")
//                }) {
//                    Image(systemName: "plus")
//                }

                ///
//                NavigationLink(destination: AddProductOptions()) {
//                    Image(systemName: "plus")
//                }
                HStack{
                    Button(action: {
                        print("Add is tapped")
                        print("PRODUCTS TAPPED !!!!\(self.products)")
                        self.addProduct.toggle()
//                        self.loaded = false
//                        self.products = []
//                        print("value of load is now \(self.loaded)")
                    }) {
    //                    Text("Add")
                        Image(systemName: "plus")
                    }.sheet(isPresented: $addProduct) {
                        AddProductOptions(warehouse: self.warehouse, lines: self.lines, unit_types: self.unit_types)
                    }//.onDisappear{self.products=[]}
                    
                    //open camera for QR scan
//                    Button(action: {
//                        self.cameraProduct.toggle()
//                    }) {
//    //                    Text("Add")
//                        Image(systemName: "camera.fill")
//                    }.sheet(isPresented: $cameraProduct) {
//                        QRCodeScanner()
//                    }
                }
                
            )
//
//        Button(action: {
//                    print("Boton add pressed")
//                }) {
//                    Image("logout")
//                    .resizable()
//                    .aspectRatio(contentMode: .fit)
//                    .frame(minWidth: 25, maxWidth: 25, minHeight: 25, maxHeight: 25, alignment: .center)
//                    .clipped()       }
        
               
    }
    
    func call_get_all_data(){
        self.getProducts()
        self.get_unitTypes()
        self.get_lines()
//        self.getSuppliers()
        
//        self.loaded() is now done here in order to tell that everythin has been loaded
//            self.loaded = true
        
        print("FINISHED LOADING EVERYTHING!!!!!!")
//            print("hello")
    }
    
    func get_unitTypes(){
        if !loaded {
                    Managers.mDataManager.getUnitTypes().getDocuments{ (documentSnapshot, err) in

                        if (err != nil) {
                            print("Error")
                            return
                        }
                        var units: [UnitType] = []
                        for document in (documentSnapshot?.documents)! {
                            print(document.data())
                            let unit = UnitType(unitTypeData: document.data())
                            units.append(unit)
                        }
                        print("UnitTYPES!!")
                        print(units)
                        self.unit_types = units
                    }
        //            self.loaded = true
                }
    }
    
    func get_lines(){
        if !loaded {
                    Managers.mDataManager.getLines().getDocuments{ (documentSnapshot, err) in

                        if (err != nil) {
                            print("Error")
                            return
                        }
                        var liness: [Line] = []
                        for document in (documentSnapshot?.documents)! {
                            print(document.data())
                            let line = Line(lineData: document.data())
                            liness.append(line)
                        }
                        print("SUPPLIERS!!")
                        print(liness)
                        self.lines = liness
                    }
        //            self.loaded = true
                }
    }
    
    func getProducts(){
//        products = []
        
        print("GETTING ALL INDEX OF WH PRODUCTS")
        print(warehouseProducts.count)
//        print("Now all indexes")
//        print(warehouseProducts)
        
//        if !loaded {
            for product in warehouseProducts{
                let amount = product["amount"] as! Float
                let productId = product["productId"] as! String
                print("PRODUCT ID")
                print(productId)
                
                Managers.mDataManager.getProduct(productId: productId).getDocument{ (document, error) in
                    if let document = document, document.exists {
                        var newProduct =  Product(productData: document.data()!)
                        newProduct.amount = amount
                        self.products.append(newProduct)
                    } else {
                        print("Document does not exist")
                    }
                }
                print("PRODUCTS DATA :!!")
                print(self.products)
                
            }
//            self.loaded = true
//        }

    }
}

struct ProductList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            ProductList(warehouseProducts: Warehouse.getDefault().products!, warehouse: Warehouse.getDefault())
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}

