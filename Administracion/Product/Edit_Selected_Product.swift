//
//  Edit_Selected_Product.swift
//  Administracion
//
//  Created by Quique Posada on 11/18/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Firebase

struct Edit_Selected_Product: View {
    var product : Product
    var warehouse : Warehouse
    var warehouseProducts : [[String : Any]]
    
    @State private var new_name : String = ""
    @State private var new_description : String = ""
    @State private var new_entryunit : String = ""
    @State private var new_outunit : String = ""
    @State private var new_line : String = ""
    
    let db = Firestore.firestore()
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
//        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
//        Text(product.name!)
//        TextField("Hello",text: $new_name)
        
        //NOTE:
        // HERE THERE CAN BE ONE OF TWO OPTIONS:
        // 1. EDIT AMOUNTS ONLY THROUGH AN INVENTORY MOVEMENT
        // 2. EDIT FROM BOTH HERE AND FROM INVENTORY MOVEMENTS
        
        VStack{
            Text("Editar Producto")//.font(.system(size: 32))
//                print("")
            
            HStack{
                Text("Nombre : ")
                TextField(product.name!, text: $new_name)
//                Text(new_name)
            }
            HStack{
                Text("Descripcion : ")
                TextField(product.description!, text: $new_description)
            }
//            Divider()
            HStack{
                Text("Unidad Entrada : ")
                TextField(product.entryUnit!, text: $new_entryunit)
            }
//            Divider()
            HStack{
                Text("Se empaca en :")
                TextField(product.outUnit!, text: $new_outunit)
            }
//            Divider()
            HStack{
                Text("Linea : ")
                TextField(product.line!, text: $new_line)
            }
            
            if(!new_description.isEmpty && !new_name.isEmpty && !new_line.isEmpty && !new_outunit.isEmpty && !new_entryunit.isEmpty){
                Button(action: {
                print("Edit tapped!")
                self.editData()
                self.presentationMode.wrappedValue.dismiss()
    //            NavigationLink(destination: ProductList(warehouse: self.warehouse))
                }){Text("Guardar Cambios")}.foregroundColor(Color.white)
                .padding()
                .background(Color.blue)
                .cornerRadius(5.0)
            }
            
            
        }.onAppear{ self.assign_Data()}.navigationBarTitle(Text("Editar Producto"))
    }
    
    
    func assign_Data(){
        new_name = product.name!
        new_description = product.description!
        new_entryunit = product.entryUnit!
        new_outunit = product.outUnit!
        new_line = product.line!
        print("WH PRODUCTS")
        print(product.id!)
        let product_array = ["productId" : product.id!, "amount": product.amount!] as [String : Any]
        print("Array to find INDEX : ")
        print(product_array)
        print("HHHHH")
        print(warehouseProducts)
//        print(warehouseProducts[0].firstIndex(where: {$0 ==  product}))
        print("Getting INDEX")
//        warehouseProducts.index{$0 == product}
//        let index = warehouseProducts.firstIndex(where: {$0})
//        let index_product = warehouseProducts.firstIndex(where: {$0.contains(where: {$0 == product})})
//        let index_product = warehouseProducts.firstIndex(where: {$0})
    }
    
    
    func editData(){
        print("Editing Data")
        let doc_to_edit_ref = db.collection("product").document(product.id!)
        
        
        // Edit Selected Product Document
        if(!new_name.isEmpty && !new_description.isEmpty && !new_line.isEmpty && !new_entryunit.isEmpty && !new_outunit.isEmpty){
        doc_to_edit_ref.updateData([
            "name": new_name,
            "description": new_description,
            "entry_unit": new_entryunit,
            "out_unit": new_outunit,
            "line": new_line,
            "updated_at": FieldValue.serverTimestamp()
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
            }
        }
        }
        else{
            print("One field is empty, cancelling operation")
        }
    }
}

struct Edit_Selected_Product_Previews: PreviewProvider {
    static var previews: some View {
        Edit_Selected_Product(product: Product.getDefault(), warehouse: Warehouse.getDefault(), warehouseProducts: Warehouse.getDefault().products!)
    }
}
