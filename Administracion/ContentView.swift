//
//  ContentView.swift
//  AIM_2
//
//  Created by Manuel Garcia on 9/22/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var store = Settings()
    @ObservedObject var Managers = ManagersObject()
    @State var showLogged = false
    @State private var warehouse : Warehouse = nil

    var body: some View {
            
//            getWarehouse(warehouseId: "bXLWlkprvBahuX11YiW7")
        
            
        return Group{

            if ((store.logged == true)){

                WarehouseList(store: self.store)
//                if(self.warehouse != nil){
//                    WarehouseDetail(warehouse: warehouse!)
//                }
                

            }else{
                LoginView(showLogged: $showLogged, store: self.store)

            }
        }

    }
    
    func getWarehouse(warehouseId: String){
        if self.warehouse == nil {

            let documentWarehouse = Managers.mDataManager.getWarehouse(warehouseId: warehouseId)
            documentWarehouse.getDocument { (document, error) in
                if let document = document, document.exists {
                    self.warehouse =  Warehouse(warehouseData: document.data()!)
                    print(document.data()!)
                } else {
                    print("Document does not exist")
                }
            }
        }

    }

}

struct LoginView: View {
    @State var password: String = ""
    @Binding var showLogged : Bool
    @State private var showingAlert = false
    @ObservedObject var store : Settings

    func login(email: String, password: String){
        showLogged = false
        UserUtils.signIn(email: email, password: password){ (result, error) in
            if error != nil {
                self.showingAlert = true
                self.store.logged = false

                print("Loggin Error")
            }else{
                print("okay")
                self.showLogged = true
                self.store.logged = true
                print("Show log \(self.showLogged) and logged \(self.store.logged)")

            }
        }
    }
    
    var body:  some View{

        VStack{
            Text("AIM")
                .font(.title)
                .multilineTextAlignment(.center)
                .lineLimit(nil)
            
            Text("Inicia sesión")
            .font(.subheadline)
            TextField("¿Cuál es tu usuario?", text: self.$store.user)
                    .padding()
                    .background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
                    .cornerRadius(4.0)
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 15, trailing: 0))

                SecureField("¿Cuál es tu contraseña?", text: $password)
                    .padding()
                    .background(Color(red: 242 / 255, green: 242 / 255, blue: 242 / 255))
                    .cornerRadius(4.0)
                    .padding(EdgeInsets(top: 0, leading: 0, bottom: 15, trailing: 0))
                
//            HStack {
//                Spacer()
//                VStack(alignment: .trailing) {
//                    Button(action: {}) {
//                        Text("Registrarse")
//                            .foregroundColor(Color.gray)
//                            .font(.subheadline)
//                            .underline()
//                    }
//                    Button(action: {}) {
//                        Text("¿Olvidaste tu contraseña?")
//                            .foregroundColor(Color.gray)
//                            .font(.subheadline)
//                            .underline()
//                    }
//                }.fixedSize(horizontal: false, vertical: true)
//                .scaledToFill()
//            }.padding()
            
            .alert(isPresented: $showingAlert) {
                Alert(title: Text("Problema al iniciar sesión"), message: Text("El usaurio o la contraseña no coinciden"), dismissButton: .default(Text("Okay")))
                
            }
            Button(action: {
                self.login(email: self.store.user, password: self.password)
                print("Show log \(self.showLogged) and logged \(self.store.logged)")
                if(self.showLogged == true){
                }
                print("Show log \(self.showLogged) and logged \(self.store.logged)")

            }) {
                Text("Iniciar Sesión")
                    .foregroundColor(Color.white)
                    .bold()

            }
            .padding()
            .background(Color.green)
            .cornerRadius(5.0)
        }.padding(30)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

