//
//  MapView.swift
//  AIM_2
//
//  Created by Manuel Garcia on 24/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI
import MapKit
import Firebase

struct MapView: UIViewRepresentable {
    var location: GeoPoint
    
    func makeUIView(context: Context) -> MKMapView {
        MKMapView(frame: .zero)
    }

    func updateUIView(_ view: MKMapView, context: Context) {
        let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude), span: span)
        view.setRegion(region, animated: true)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(location: Warehouse.getDefault().location!)
    }
}
