//
//  InventoryMovement.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class InventoryMovement{
    var movementTypeId : String?
    var warehouseId :  String?
    var supplierId : String?
    var productId : String?
    var movementType : String?
    var warehouse :  String?
    var supplier : String?
    var product : String?
    var description : String?
    var amount : Float?
    var createdAt : Timestamp?
    
    var warehouse_to_send :  String?
    var warehouse_to_send_id :  String?

    
    var id : String?
    
    //temporalmente se quitan suppliers de inventory movements
    init(inventoryMovementData : [String:Any]) {
        //id
        id = inventoryMovementData["id"] as? String
        movementType = (inventoryMovementData["movement_type"] as? String)
         warehouse =  (inventoryMovementData["warehouse"] as? String)
         supplier = (inventoryMovementData["supplier"] as? String)
         product =  (inventoryMovementData["product"] as? String)
        
        movementTypeId = CommonUtils.getIdFromReference(data: (inventoryMovementData["movement_type_id"] as! DocumentReference))
        warehouseId = CommonUtils.getIdFromReference(data: (inventoryMovementData["warehouse_id"] as! DocumentReference))
        supplierId = CommonUtils.getIdFromReference(data: (inventoryMovementData["supplier_id"] as! DocumentReference))
        productId = CommonUtils.getIdFromReference(data: (inventoryMovementData["product_id"] as! DocumentReference))
        description = inventoryMovementData["description"] as? String
        amount = inventoryMovementData["amount"] as? Float
        createdAt = inventoryMovementData["created_at"] as? Timestamp
    }
    static func getDefault() -> InventoryMovement{
        return InventoryMovement(inventoryMovementData: ["movement_type":"Entrada", "warehouse": "Norte"])
    }
    
    func toString() -> String{
        return "Mov \(self.movementType ?? "nada") \n War \(warehouse ?? "nada")\nProduct \(product ?? "nada") \n Supp \(supplier ?? "nada")  "
    }
    
    func toStringId() -> String{
        return "Mov \(self.movementTypeId ?? "nada") \n War \(self.warehouseId ?? "nada")\nProduct \(self.productId ?? "nada") \n Supp \(self.supplierId ?? "nada")  "
    }
    
}

extension InventoryMovement: Identifiable {
    var identify: InventoryMovement { self }
}

