//
//  MovementType.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class MovementType{
    var name : String?
    var id : String?
    
    init(movementTypeData : [String: Any]) {
        name = movementTypeData["name"] as? String
        id = movementTypeData["id"] as? String
    }
    
    static func getDefault() -> MovementType{
        return MovementType(movementTypeData: ["name": "Entrada"])
    }
}

extension MovementType: Identifiable {
    var identify: MovementType { self }
}

