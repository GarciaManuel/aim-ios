//
//  Warehouse.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class Warehouse{
    var id : String?
    var name : String?
    var imageId : String?
    var address : String?
    var state : String?
    var products : [[String : Any]]?
    var location : GeoPoint?
    var createdAt : Timestamp?
    var updatedAt :  Timestamp?
    
    init(warehouseData : [String: Any]) {
        id = warehouseData["id"] as? String
        name = warehouseData["name"] as? String
        imageId = warehouseData["imageId"] as? String
        address = warehouseData["address"] as? String
        state = warehouseData["state"] as? String
        location = warehouseData["location"] as? GeoPoint
        createdAt = warehouseData["created_at"] as? Timestamp
        updatedAt = warehouseData["updated_at"] as? Timestamp
        products = []
        for product in ((warehouseData["products"] as? [Any])!){
            let newProduct = product as! [String: Any]
            products!.append(
                [
                    "productId": CommonUtils.getIdFromReference(data: (newProduct["product"] as! DocumentReference)) ,
                    "amount": (newProduct["amount"] as! Float)
                    // commented for now since firebase DOES NOT SUPPORT having FieldValue.servertimestamp() inside ARRAYS
//                    "createdAt" : (newProduct["created_at"] as! Timestamp),
//                    "updatedAt" : (newProduct["updated_at"] as! Timestamp)
                ]
            )
        }
        
    }
    
    static func getDefault() -> Warehouse{
        return Warehouse(warehouseData: ["id": 1, "name": "Almacen 2", "address": "Tec", "state_id": "QRO"])
    }
    
    

}

extension Warehouse: Identifiable {
    var identify: Warehouse { self }
}

