//
//  Line.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class Line{
    var id : String?
    var name : String?
    var createdAt : Timestamp?
    var updatedAt : Timestamp?
    
    init(lineData: [String:Any?]) {
        id = lineData["id"] as? String
        name = lineData["name"] as? String
        createdAt = lineData["created_at"] as? Timestamp
        updatedAt = lineData["updated_at"] as? Timestamp
    }
    
    static func getDefault() -> Line{
        return Line(lineData: ["id": 1, "name": "Quimico 2", "description": "DefaultTest"])
    }
}
