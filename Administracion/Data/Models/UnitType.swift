//
//  UnitType.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class UnitType{
    var name : String?
    var createdAt  : Timestamp?
    var updatedAt  : Timestamp?
    
    init(unitTypeData: [String: Any]){
        name = unitTypeData["name"] as? String
        createdAt = unitTypeData["created_at"] as? Timestamp
        updatedAt = unitTypeData["updated_at"] as? Timestamp
    }
    
    static func getDefault() -> UnitType{
        return UnitType(unitTypeData: ["id": "12132312312", "name": "Litros"])
    }
}
extension UnitType: Identifiable {
    var identify: UnitType { self }
}
