//
//  Product.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class Product{
    var ref : String?
    var id : String?
    var name : String?
    var description : String?
//    var lineId : String?
    var line : String?
    var amount : Float?
    var entryUnit : String?
    var outUnit : String?
    var createdAt : Timestamp?
    var updatedAt : Timestamp?
    var aux: String?
    var imageId: String?
    
    init(productData : [String:Any]) {
        ref = productData["ref"] as? String
        id = productData["id"] as? String
        name = productData["name"] as? String
        description = productData["description"] as? String
//        lineId = CommonUtils.getIdFromReference(data: productData["line"] as? DocumentReference)
        line = productData["line"] as? String
        amount = productData["amount"] as? Float
        entryUnit = productData["entry_unit"] as? String
        outUnit = productData["out_unit"] as? String
        createdAt = productData["created_at"] as? Timestamp
        updatedAt = productData["updated_at"] as? Timestamp
        imageId = productData["imageId"] as? String

    }
    
    static func getDefault() -> Product{
        return Product(productData: ["ref": "mTLfC9ydwajqbikLYULY","id": "1", "name": "Cloroformo", "description": "Nuevo producto", "line": "Químicos", "amount": 10.0, "entry_unit": "Litros", "out_unit": "Galones"])
    }
    
    
}
extension Product: Identifiable {
    var identify: Product { self }
}

