//
//  Supplier.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class Supplier{
    var name : String?
    var imageId : String?
    var bankAccount : String?
    var rfc : String?
    var address : String?
    var email : String?
    var phoneNumber : String?
    var preferredPaymentMethod : String?
    var createdAt : Timestamp?
    var updatedAt : Timestamp?
    var products : [[String: Any]]?
    
    var id : String?
    
    init(supplierData : [String: Any]) {
        name = supplierData["name"] as? String
        imageId = supplierData["imageId"] as? String
        
        id = supplierData["id"] as? String

        bankAccount = supplierData["bank_account"] as? String
        rfc = supplierData["rfc"] as? String
        address = supplierData["address"] as? String

        email = supplierData["email_address"] as? String
        phoneNumber = supplierData["phone_number"] as? String
        preferredPaymentMethod = supplierData["preferred_payment_method"] as?  String
        createdAt = supplierData["created_at"] as? Timestamp
        updatedAt = supplierData["updated_at"] as? Timestamp
        products = []
        
        //supplier for products seems to be deprecated
//        for product in ((supplierData["products"] as? [Any])!){
//            let product = product as! [String: Any]
//            products!.append(
//                [
//                    "productId": CommonUtils.getIdFromReference(data: (product["product"] as! DocumentReference)) ,
//                    "price": (product["price"] as! Float),
//                    "isPrimary": (product["is_primary"] as! Bool),
//                    "fiability": (product["fiability"] as! Float),
//                    "deliveryDays" : (product["delivery_days"] as! Int)
//                ]
//            )
//        }
        
    }
    static func getDefault() -> Supplier{
        return Supplier(supplierData: ["name": "Prolimp S.A de C.V", "email": "prolimp@contacto.mx"])
    }
    
}
extension Supplier: Identifiable {
    var identify: Supplier { self }
}

