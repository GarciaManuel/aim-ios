//
//  DataManager.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseAuth

class DataManager {
    
    var mFirestoreHelper: FirestoreHelper?
    var mFirestore: Firestore?
    
    init()
    {
        mFirestoreHelper = AppFirestoreHelper()
        mFirestore = Firestore.firestore()
        
        let settings = mFirestore?.settings
        settings?.areTimestampsInSnapshotsEnabled = true
        mFirestore?.settings = settings!
    }
    
    func getLoadProductsRef() -> Query{
        return mFirestoreHelper!.loadProductsRef()
    }
    func getLineById(lineId: String) -> DocumentReference{
        return mFirestoreHelper!.getLineByID(lineId: lineId)
    }
    func getUnitTypeById(unitTypeId: String) -> DocumentReference{
        return mFirestoreHelper!.getUnitTypeById(unitTypeId: unitTypeId)
    }
    func getWarehouses() -> Query {
        return mFirestoreHelper!.getWarehouses()

    }
    
    //Get all movement types
    func getAll_MovementTypes() -> Query {
        return mFirestoreHelper!.getAll_MovementTypes()
    }
    
    func getLines() -> Query{
        return mFirestoreHelper!.getLines()
    }
    
    
    func getWarehouse(warehouseId: String) -> DocumentReference {
        return mFirestoreHelper!.getWarehouse(warehouseId: warehouseId)
    }
    func getUnitTypes() -> Query {
        return mFirestoreHelper!.getUnitTypes()
    }
    func getProduct(productId: String) -> DocumentReference {
        return mFirestoreHelper!.getProduct(productId: productId)
    }
    func getSuppliers() -> Query{
        return mFirestoreHelper!.getSuppliers()
    }
    func getInventoryMovements() -> Query{
        return mFirestoreHelper!.getInventoryMovements()
    }

}
