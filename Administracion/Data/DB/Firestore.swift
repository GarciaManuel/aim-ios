//
//  Firestore.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//


import Foundation

import Firebase
import FirebaseFirestore
import FirebaseUI
import FirebaseStorage

class AppFirestoreHelper: FirestoreHelper {

    
    
    func getFirestore() -> Firestore {
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        return db
    }
    
    
    func loadProductsRef() -> Query {
        return getFirestore().collection("product")
    }
    
    func getLineByID(lineId: String) -> DocumentReference {
        return getFirestore().collection("line").document(lineId);
    }
    
    func getUnitTypeById(unitTypeId: String) -> DocumentReference {
        return getFirestore().collection("unit_type").document(unitTypeId);
    }
    
     func getWarehouses() -> Query {
           return getFirestore().collection("warehouse")
    }
    
    //Get all movement types
    
    func getAll_MovementTypes() -> Query{
        return getFirestore().collection("movement_type")
    }
    
    func getLines() -> Query {
        return getFirestore()
            .collection("line")
        
    }
    
    func getWarehouse(warehouseId: String) -> DocumentReference {
        return getFirestore().document("warehouse/" + warehouseId)
    }
    func getUnitTypes() -> Query{
        return getFirestore().collection("unit_type")
    }
    func getProduct(productId: String) -> DocumentReference{
        return getFirestore().document("product/" + productId)
    }
    func getSuppliers() -> Query{
        return getFirestore().collection("supplier")
    }
    func getInventoryMovements() -> Query{
        return getFirestore().collection("inventory_movements")
    }


}
