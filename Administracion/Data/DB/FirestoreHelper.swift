//
//  FirestoreHelper.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Firebase
import FirebaseFirestore
import FirebaseStorage
import FirebaseUI

protocol FirestoreHelper {
    func loadProductsRef() -> Query
    func getLineByID(lineId: String) -> DocumentReference
    func getUnitTypeById(unitTypeId: String) -> DocumentReference
    func getWarehouses() -> Query
    
    //get all movement types
    func getAll_MovementTypes() -> Query
    
    func getLines() -> Query
    
    func getWarehouse(warehouseId: String) -> DocumentReference
    func getUnitTypes() -> Query
    func getProduct(productId: String) -> DocumentReference
    func getSuppliers() -> Query
    func getInventoryMovements() -> Query
}
