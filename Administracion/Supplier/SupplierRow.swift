//
//  SupplierRow.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct SupplierRow: View {
    var supplier: Supplier
    
    var body: some View {
        HStack {
            FirebaseImage(id: supplier.imageId!)
               .frame(width: 50, height: 50)
                .padding(.leading,10)
                
            VStack {
                HStack {
                    Text(supplier.name!)
                    Spacer()
                }
                
                HStack {
                    Text(supplier.email!)
                        .font(.caption)
                    Spacer()
                }
            }
            .fixedSize(horizontal: false, vertical: true)
            .padding(.leading,10)
            
        }
    }
}

struct SupplierRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SupplierRow(supplier: Supplier.getDefault())
            SupplierRow(supplier: Supplier.getDefault())
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
